
<!doctype html>
<html lang="en">
<head>
    <?php
    include "includes/header.php";
    ?>
</head>
<body>
<!-- :::::::::: Header Section Start :::::::: -->
<header>
    <div class="container mb-5">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <?php include "includes/navbar.php"; ?>
            </div>
        </div>
    </div>
</header>



<!-- :::::::::: Page Banner Section Start :::::::: -->

<!-- ::::::::::: Page Banner Section End ::::::::: -->



<!-- :::::::::: Blog With Right Sidebar Start :::::::: -->


                <?php
                if (isset($_GET['spid'])){
                     $singlePostId = $_GET['spid'];

                     $singlePostQuery = "SELECT * FROM posts WHERE status = 1 AND id='$singlePostId'";

                    /** @var TYPE_NAME $dbConnection */
                    $singlePostStmt = mysqli_query($dbConnection, $singlePostQuery);
                     $postItem = mysqli_fetch_assoc($singlePostStmt);
                      $postTitle = $postItem['title'];
                     $postDescription = $postItem['description'];
                     $image = $postItem['image'];
                     $categoryId = $postItem['category_id'];
                     $authorId = $postItem['author_id'];
                     $tags = $postItem['tags'];
                     $status = $postItem['status'];
                     $pDate = $postItem['p_date'];
                     $creationTime =  date("D-M-Y", strtotime($pDate));
                      $creationTime;

                      $categoryNameQuery = "SELECT cat_title FROM categories WHERE status = 1 AND id='$categoryId'";
                $categoryNameStmt = mysqli_query($dbConnection,$categoryNameQuery);
                $categoryName = mysqli_fetch_assoc($categoryNameStmt);

               ?>
                    <section class="blog-bg background-img">
                        <div class="container">
                            <!-- Row Start -->
                            <div class="row">
                                <div class="col-md-12">
                                    <h2 class="page-title">Single Blog Page</h2>
                                    <!-- Page Heading Breadcrumb Start -->
                                    <nav class="page-breadcrumb-item">
                                        <ol>
                                            <li><a href="=index.php">Home <i class="fa fa-angle-double-right"></i></a></li>
                                            <li><a href="">Blog <i class="fa fa-angle-double-right"></i></a></li>
                                            <!-- Active Breadcrumb -->
                                            <li class="active">Single Left Sidebar</li>
                                        </ol>
                                    </nav>
                                    <!-- Page Heading Breadcrumb End -->
                                </div>

                            </div>
                            <!-- Row End -->
                        </div>
                    </section>
                    <section>
                        <div class="container">
                            <div class="row">

                                <!-- Blog Single Posts -->
                                <div class="col-md-8">
                <div class="blog-single">
                    <!-- Blog Title -->
                    <a href="#">
                        <h3 class="post-title"><?php echo $postTitle; ?></h3>
                    </a>

                    <!-- Blog Categories -->
                    <div class="single-categories">

                        <h6 >
                            <?php
                            $allCat = explode(",",$categoryId);
                            foreach ($allCat as $catId){
                                $catReadQuery  = "SELECT * FROM categories WHERE id = '$catId'";
                                $catReadStmt = mysqli_query($dbConnection,$catReadQuery);
                                while ($catRow = mysqli_fetch_assoc($catReadStmt)){
                                    $catId = $catRow['id'];
                                    $catTitle = $catRow['cat_title'];
                                    ?>
                                    <span  class="mx-1 text-white bg-warning"><a class="text-white" href="category.php?catid=<?php echo $catId;?>"><?php echo $catTitle; ?></a></span>
                                    <?php
                                }
                            }
                            ?>
                        </h6>
                    </div>

                    <!-- Blog Thumbnail Image Start -->
                    <div class="blog-banner">
                        <a href="#">
                            <img src="assets/image/upload/post/<?php echo $image; ?>">
                        </a>
                    </div>
                    <!-- Blog Thumbnail Image End -->

                    <!-- Blog Description Start -->
                    <?php echo  $postDescription; ?>
                    <!-- Blog Description End -->
                </div>

                <!-- Single Comment Section Start -->
                <div class="single-comments">
                    <!-- Comment Heading Start -->
                    <?php


                    $showCommentQuery = "SELECT * FROM comments WHERE post_id = '$singlePostId' AND status = 1 ORDER BY comment_date ASC ";
                    $showCommentStmt = mysqli_query($dbConnection,$showCommentQuery);
                    $totalComments = mysqli_num_rows($showCommentStmt);

                    ?>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <h4>All Latest Comments (<?php echo  $totalComments ?>)</h4>
                            <div class="title-border"></div>
                        </div>
                    </div>
                    <!-- Comment Heading End -->
<?php


$showCommentQuery = "SELECT * FROM comments WHERE post_id = '$singlePostId' AND status = 1 ORDER BY comment_date ASC ";
$showCommentStmt = mysqli_query($dbConnection,$showCommentQuery);
$totalComments = mysqli_num_rows($showCommentStmt);
if ($totalComments == 0){
    ?>
    <div class="alert alert-warning">
        No Comments Found!
    </div>
    <?php
}else{
    while ($row = mysqli_fetch_assoc($showCommentStmt)){
        $commentsId = $row['id'];
        $postId = $row['post_id'];
         $commentorId = $row['user_id'];
        $comment = $row['comments'];
        $status = $row['status'];
        $commentDate = $row['comment_date'];
        $changeDate = date("D-M-Y", strtotime($commentDate));


        $commentUserRead = "SELECT * FROM users WHERE id = '$commentorId' AND status = 1";
        $commentUserStmt = mysqli_query($dbConnection,$commentUserRead);
        while ($userRow = mysqli_fetch_assoc($commentUserStmt)){

            $userName = $userRow['name'];
            $userImage = $userRow['image'];


        ?>
        <!-- Single Comment Post Start -->
        <div class="row each-comments">
            <div class="col-md-2">
                <!-- Commented Person Thumbnail -->
                <div class="comments-person">
                    <img src="assets/image/upload/users/<?php echo $userImage; ?>">
                </div>
            </div>

            <div class="col-md-10 no-padding">
                <!-- Comment Box Start -->
                <div class="comment-box">
                    <div class="comment-box-header">
                        <ul>
                            <li class="post-by-name"><?php echo $userName; ?></li>

                            <?php
if (!empty($_SESSION['id'])){
    if ($_SESSION['id'] == $commentorId){
        ?>
        <li class="post-by-name">
            <a href="singlePage.php?ec=edit-comment&cid=<?php echo $commentsId ?>" class="text-warning"><i class="fa fa-edit"></i></a>
        </li>
        <li class="post-by-name">
            <a href="singlePage.php?ec=delete-comment&cid=<?php echo $commentsId ?>" class="text-danger"><i class="fa fa-trash"></i></a>
        </li>
        <?php
    }
}

                                ?>

                            <li class="post-by-hour"><?php echo  $changeDate?></li>
                        </ul>
                    </div>
                    <p><?php echo $comment; ?></p>
                </div>
                <!-- Comment Box End -->
            </div>
        </div>
        <!-- Single Comment Post End -->
        <?php
        }
    }
}
?>
                </div>
                <!-- Single Comment Section End -->



                <?php
                if (empty($_SESSION['id'])){
                    ?>
                    <div class="alert alert-warning">
                        Please login to post a comment in this post.
                    </div>

                    <form action="" method="POST" class="contact-form">
                        <!-- Left Side Start -->
                        <div class="row">
                            <div class="col-md-6"  >
                                <div class="form-group">
                                    <input type="email" name="email" placeholder="Email Address" class="form-input" autocomplete="off" required="required">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" placeholder="Subject" class="form-input" autocomplete="off" required="required">
                                    <i class="fa fa-diamond"></i>
                                </div>
                                <button type="submit" name="front_login" class="btn-main"><i class="fa fa-user"></i> LogIn</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                    </form>
                    <?php
                    if (isset($_POST['front_login'])){
                        if (!empty($_POST['email']) && !empty($_POST['password'])) {
                            $email = mysqli_real_escape_string($dbConnection,$_POST['email']);
                            $password =  mysqli_real_escape_string($dbConnection, $_POST['password']);
                            $hasedPass = sha1($password);
                            $query = "SELECT * FROM users WHERE email= '$email'  AND  password='$hasedPass' ";
                            $statement = mysqli_query($dbConnection, $query);
                            $countUser = mysqli_num_rows($statement);
                            if ($countUser == 0) {
                                $_SESSION['msg'] = "Credential Does Not Match";
                            } else {
                                while ($row = mysqli_fetch_array($statement)) {
                                    $_SESSION['role'] = $row['role'];
                                    $_SESSION['id'] = $row['id'];
                                    $_SESSION['name'] = $row['name'];
                                    $_SESSION['email'] = $row['email'];
                                    $password = $row['password'];
                                    if ($email == $_SESSION['email'] && $hasedPass == $password) {
                                        header('Location:index.php');
                                    } elseif ($email != $_SESSION['email'] || $password != $hasedPass) {
                                        $_SESSION['msg'] = "Credential Does Not Match";
                                        header('Location:index.php');

                                    }else {
                                        $_SESSION['msg'] = "Sorry You Are Not Admin or Editor";
                                        header("refresh:1; url=index.php");
                                    }
                                }
                            }
                        }else{
                            $_SESSION['msg'] = "Credential Does Not Match";
                        }
                    }


                    ?>
                    <?php
                }else{
                    ?>
                    <!-- Post New Comment Section Start -->
                    <div class="post-comments">
                        <h4>Post Your Comments</h4>
                        <div class="title-border"></div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                        <div class="error_message">
                            <?php
                            if (!empty($_SESSION['msg'])) {
                                echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
                                unset($_SESSION['msg']);
                            } elseif (!empty($_SESSION['success_msg'])) {
                                echo '<div class="alert alert-success">' . $_SESSION['success_msg'] . '</div>';
                                unset($_SESSION['success_msg']);
                            }
                            ?>
                        </div>
                        <!-- Form Start -->
                        <form method="POST" class="contact-form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="comments" style="resize: none;"  class="form-input" placeholder="Your Comments Here..."></textarea>
                                        <i class="fa fa-pencil-square-o"></i>
                                    </div>
                                    <!-- Post Comment Button -->
                                    <button type="submit" name="post_comment" class="btn-main"><i class="fa fa-paper-plane-o"></i> Post Your Comments</button>


                                </div>
                            </div>
                        </form>
                        <?php
                        if (isset($_POST['post_comment'])){
                            $postComment = mysqli_real_escape_string($dbConnection,$_POST['comments']);
                            $userId = $_SESSION['id'];

                 if (!empty($postComment)){
                           $commentQuery = "INSERT INTO comments(post_id,user_id,comments,status)VALUES ('$singlePostId', '$userId', '$postComment','1')";
                             $commentStmt = mysqli_query($dbConnection,$commentQuery);
                             if ($commentStmt){
                                   $_SESSION['success_msg'] = "Comment Posted Successfully";
                                   header("Location:singlePage.php?spid=$singlePostId");

                             }else{
                                  'failed'.mysqli_error($dbConnection);
                             }
                         }


                        }


                        ?>
                    </div>
                    <!-- Post New Comment Section End -->
                <?php
                }
                ?>

            </div>


            <?php
            include "includes/sidebar.php";
            ?>


        </div>
    </div>
</section>

<?php

}elseif(isset($_GET['ec'])){
                    $ec = $_GET['ec'];
                    if ($ec == "edit-comment"){
                        if (!empty($_SESSION['id'])){

                       ?>

                            <div class="container ">
                                <div class="row">
                                    <div class="col-md-8 mx-auto">
                                        <div class="card mt-2">
                                            <div class="card-header">
                                                <h3>Edit Your Comment</h3>
                                            </div>
                                            <?php
                                            if (isset($_GET['cid'])){
                                                 $comId = $_GET['cid'];
                                                 ?>
                                                <div class="card-body">

                                                    <?php
                                                     $editComQuery = "SELECT * FROM comments WHERE id = '$comId'";
                                                    /** @var TYPE_NAME $dbConnection */
                                                    $editComStmt = mysqli_query($dbConnection,$editComQuery);
                                                    while ($comRow = mysqli_fetch_array($editComStmt)){
                                                         $comment = $comRow['comments'];

                                                         ?>
                                                        <form method="POST" class="contact-form">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <textarea name="comments" style="resize: none;"  class="form-input" placeholder="Your Comments Here..."><?php echo $comment; ?></textarea>
                                                                        <i class="fa fa-pencil-square-o"></i>
                                                                    </div>
                                                                    <!-- Post Comment Button -->
                                                                    <button type="submit" name="edit_comment" class="btn-main btn-block"><i class="fa fa-paper-plane-o"></i> Save Changes</button>


                                                                </div>
                                                            </div>
                                                        </form>
                                                        <?php
                                                        if (isset($_POST['edit_comment'])){
                                                          $comment = $_POST['comments'];
                                                          $updateComQuery = "UPDATE comments SET comments = '$comment' WHERE id='$comId'";
                                                          $updateComStmt = mysqli_query($dbConnection,$updateComQuery);
                                                          if ($updateComStmt){
                                                             header("Location:singlePage.php?ec=edit-comment&cid=$comId");
                                                          }else{
                                                              echo 'failed'.mysqli_error($dbConnection);
                                                          }
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                    ?>

                                                </div>
                                                <?php
                                            }
                                            ?>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        <?php

                        }else{
                            ?>
                            <div class="container ">
                                <div class="row">
                                    <div class="col-md-8 mx-auto">
                                        <div class="alert alert-danger">Sorry! Page Not Found</div>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                    }elseif ($ec == "delete-comment"){
if (isset($_GET['cid'])){

    $comId = $_GET['cid'];
    $deleteComQuery = "DELETE FROM comments WHERE id = '$comId'";

    /** @var TYPE_NAME $dbConnection */
    $deleteComStmt = mysqli_query($dbConnection,$deleteComQuery);
    if ($deleteComStmt){
        header("Location:index.php");
    }
}
                    }
                }


?>

<!-- ::::::::::: Blog With Right Sidebar End ::::::::: -->
<!-- :::::::::: Footer Section Start :::::::: -->
<footer class="mt-5">
    <?php
    include "includes/footer.php";
    ?>
</footer>
<!-- ::::::::::: Footer Section End ::::::::: -->

<?php include 'includes/script.php'; ?>
</body>
</html>
