
<!doctype html>
<html lang="en">
<head>
    <?php
    include "includes/header.php";
    ?>
</head>
<body>
<!-- :::::::::: Header Section Start :::::::: -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php include "includes/navbar.php"; ?>
            </div>
        </div>
    </div>
</header>

<section>
    <div class="login-sec">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="error_message">
                    <?php
                    if (!empty($_SESSION['msg'])) {
                        echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
                        unset($_SESSION['msg']);
                    }
                    ?>
                </div>
                <form action="" method="POST" class="contact-form">
                    <!-- Left Side Start -->
                    <div class="row">
                        <div class="col-md-12"  >
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Email Address" class="form-input" autocomplete="off" required="required">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" placeholder="Subject" class="form-input" autocomplete="off" required="required">
                                <i class="fa fa-diamond"></i>
                            </div>
                            <button type="submit" name="front_login" class="btn-main btn-block"><i class="fa fa-user"></i> LogIn</button>
                            <a href="fregister.php" class="text-white btn-main btn-block text-center"><i class="fa fa-user-plus"></i> Register</a>
                        </div>
                    </div>
                </form>
                <?php
                /** @var TYPE_NAME $_POST */
                if (isset($_POST['front_login'])){
                    if (!empty($_POST['email']) && !empty($_POST['password'])) {
                        /** @var TYPE_NAME $dbConnection */
                        $email = mysqli_real_escape_string($dbConnection,$_POST['email']);
                        $password =  mysqli_real_escape_string($dbConnection, $_POST['password']);
                        $hasedPass = sha1($password);
                        $query = "SELECT * FROM users WHERE email= '$email'  AND  password='$hasedPass' ";
                        $statement = mysqli_query($dbConnection, $query);
                        $countUser = mysqli_num_rows($statement);
                        if ($countUser == 0) {
                            $_SESSION['msg'] = "No User Found.Please Register";
                            header('Location:fregister.php');
                        } else {
                            while ($row = mysqli_fetch_array($statement)) {
                                $_SESSION['role'] = $row['role'];
                                $_SESSION['id'] = $row['id'];
                                $_SESSION['name'] = $row['name'];
                                $_SESSION['email'] = $row['email'];
                                $password = $row['password'];
                                if ($email == $_SESSION['email'] && $hasedPass == $password) {
                                    header('Location:index.php');
                                } elseif ($email != $_SESSION['email'] || $password != $hasedPass) {
                                    $_SESSION['msg'] = "Credential Does Not Match";
                                    header('Location:flogin.php');

                                }
                            }
                        }
                    }else{
                        $_SESSION['msg'] = "Please Enter Email or Password Properly";
                    }
                }


                ?>
            </div>

        </div>
    </div>
</section>

<!-- :::::::::: Footer Section Start :::::::: -->
<footer>
    <?php
    include "includes/footer.php";
    ?>
</footer>
<!-- ::::::::::: Footer Section End ::::::::: -->

<?php include 'includes/script.php'; ?>
</body>
</html>
