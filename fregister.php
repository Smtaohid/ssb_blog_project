
<!doctype html>
<html lang="en">
<head>
    <?php
    include "includes/header.php";
    ?>
</head>
<body>
<!-- :::::::::: Header Section Start :::::::: -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php include "includes/navbar.php"; ?>
            </div>
        </div>
    </div>
</header>

<section>
    <div class="login-sec mt-5">
        <div class="row">
            <div class="col-md-8 mx-auto">

                <div class="card shadow">
                    <div class="card-header bg-transparent">
                        <h3>Register</h3>
                    </div>
                    <div class="card-body p-5">
                        <div class="error_message">
                            <?php
                            if (!empty($_SESSION['msg'])) {
                                echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
                                unset($_SESSION['msg']);
                            }
                            ?>
                        </div>
                        <form action="" method="POST" class="contact-form">
                            <!-- Left Side Start -->
                            <div class="row">
                                <div class="col-md-12"  >

                                    <div class="form-group">
                                        <input type="text" name="name" placeholder="Full Name" class="form-input" autocomplete="off" required="required">
                                        <i class="fa fa-file"></i>
                                    </div>

                                    <div class="form-group">
                                        <input type="email" name="email" placeholder="Email Address" class="form-input" autocomplete="off" required="required">
                                        <i class="fa fa-envelope-o"></i>
                                    </div>


                                    <div class="form-group">
                                        <input type="password" name="password" placeholder="Password" class="form-input" autocomplete="off" required="required">
                                        <i class="fa fa-diamond"></i>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="confirm_password" placeholder="Confirm Password" class="form-input" autocomplete="off" required="required">
                                        <i class="fa fa-diamond"></i>
                                    </div>
                                    <button type="submit" name="front_register" class="btn-main btn-block"><i class="fa fa-user"></i> LogIn</button>
                                    <a href="flogin.php" class="btn rounded-0 btn-outline-danger btn-block text-center"><i class="fa fa-user-plus"></i> Already Have an Account ?</a>
                                </div>
                            </div>
                        </form>



                        <?php
                        if (isset($_POST['front_register'])) {

                            /** @var TYPE_NAME $_POST */
                            if (!empty($_POST['name']) || !empty($_POST['email']) || !empty($_POST['password']) || !empty($_POST['confirm_password'])) {
                                /** @var TYPE_NAME $dbConnection */
                                $name = mysqli_real_escape_string($dbConnection, $_POST['name']);
                                $email = mysqli_real_escape_string($dbConnection, $_POST['email']);
                                $password = mysqli_real_escape_string($dbConnection, $_POST['password']);
                                $confirmPassword = mysqli_real_escape_string($dbConnection, $_POST['confirm_password']);
                                $query = "SELECT * FROM users WHERE email='$email'";
                                $statement = mysqli_query($dbConnection, $query);

                                $countUser = mysqli_num_rows($statement);

                                if ($countUser == 0) {
                                    if ($password == $confirmPassword) {

                                        if (strlen($password) <= 7 || strlen($password) > 100) {
                                            $_SESSION['msg'] = "Password must be at least 8 characters";
                                        } else {
                                            $hasedPass = sha1($password);


                                            $query = "INSERT INTO users(name,email,password,join_date)VALUES('$name','$email','$hasedPass',now())";
                                            $statement = mysqli_query($dbConnection, $query);
                                            if ($statement) {
                                                $_SESSION['success_msg'] = "Registered Successfully";
                                                header("refresh:1; url=flogin.php");
                                            } else {
                                                $_SESSION['msg'] = "Opps!! Insert Your Data Perfectly";
                                            }
                                        }
                                    } else {
                                        $_SESSION['msg'] = "Password Doesn't Match";
                                    }
                                } else {
                                    $_SESSION['msg'] = "This Email Is Already Registered ";
                                }
                            } else {
                                $_SESSION['msg'] = "Opps!! Insert Your Data Perfectly";
                            }
                        }
                        ?>
                    </div>
                </div>


            </div>

        </div>
    </div>
</section>

<!-- :::::::::: Footer Section Start :::::::: -->
<footer>
    <?php
    include "includes/footer.php";
    ?>
</footer>
<!-- ::::::::::: Footer Section End ::::::::: -->

<?php include 'includes/script.php'; ?>
</body>
</html>
