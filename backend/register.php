<?php include('inc/dbConn.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Register</title>

    <!-- Custom fonts for this template-->
    <link href="../assets/backend/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../assets/backend/css/sb-admin-2.min.css" rel="stylesheet">

</head>
<?php
if (isset($_POST['regiseter'])) {

    if (!empty($_POST['name']) || !empty($_POST['email']) || !empty($_POST['password']) || !empty($_POST['confirm_password'])) {
        $name = mysqli_real_escape_string($dbConnection, $_POST['name']);
        $email = mysqli_real_escape_string($dbConnection, $_POST['email']);
        $password = mysqli_real_escape_string($dbConnection, $_POST['password']);
        $confirmPassword = mysqli_real_escape_string($dbConnection, $_POST['confirm_password']);
        $query = "SELECT * FROM users WHERE email='$email'";
        $statement = mysqli_query($dbConnection, $query);

        $countUser = mysqli_num_rows($statement);

        if ($countUser == 0) {
            if ($password == $confirmPassword) {

                if (strlen($password) <= 7 || strlen($password) > 100) {
                    $_SESSION['msg'] = "Password must be at least 8 characters";
                } else {
                    $hasedPass = sha1($password);


                    $query = "INSERT INTO users(name,email,password,join_date)VALUES('$name','$email','$hasedPass',now())";
                    $statement = mysqli_query($dbConnection, $query);
                    if ($statement) {
                        $_SESSION['success_msg'] = "Registered Successfully";
                        header("refresh:1; url=register.php");
                    } else {
                        $_SESSION['msg'] = "Opps!! Insert Your Data Perfectly";
                    }
                }
            } else {
                $_SESSION['msg'] = "Password Doesn't Match";
            }
        } else {
            $_SESSION['msg'] = "This Email Is Already Registered ";
        }
    } else {
        $_SESSION['msg'] = "Opps!! Insert Your Data Perfectly";
    }
}
?>

<body class="bg-gradient-primary">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            </div>
                            <div class="error_message">
                                <?php
                                if (!empty($_SESSION['msg'])) {
                                    echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
                                    unset($_SESSION['msg']);
                                } elseif (!empty($_SESSION['success_msg'])) {
                                    echo '<div class="alert alert-success">' . $_SESSION['success_msg'] . '</div>';
                                    unset($_SESSION['success_msg']);
                                }
                                ?>
                            </div>
                            <form class="user" method="POST">
                                <div class="form-group row">
                                    <div class="col-md-12 mb-3 mb-sm-0">
                                        <input type="text" name="name" class="form-control form-control-user" id="exampleFirstName" placeholder="Full Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email Address">
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" name="confirm_password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repeat Password">
                                    </div>
                                </div>
                                <input type="submit" name="regiseter" value="Register" class="btn btn-primary btn-user btn-block">
                                <hr>
                                <a href="index.html" class="btn btn-google btn-user btn-block">
                                    <i class="fab fa-google fa-fw"></i> Register with Google
                                </a>
                                <a href="index.html" class="btn btn-facebook btn-user btn-block">
                                    <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                                </a>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="forgot-password.html">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="index.php">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../assets/backend/vendor/jquery/jquery.min.js"></script>
    <script src="../assets/backend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../assets/backend/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../assets/backend/js/sb-admin-2.min.js"></script>

</body>

</html>