<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include('inc/head.php')
    ?>

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include('inc/sitebar.php') ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <?php include('inc/topbar.php') ?>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Manage All Posts</h1>
    </div>

    <?php
    $do = $_GET['do'] ?? 'manage';
   if ($do == 'manage'){ ?>
    <table class="table table-bordered">
        <table class="table  table-bordered text-center ">
            <thead class="thead-dark">
            <tr>
                <th scope="col">SL</th>
                <th scope="col">Title</th>
                <th scope="col">Photo</th>
                <th scope="col">Tags</th>
                <th scope="col">Status</th>
                <th scope="col">Author Name</th>
                <th scope="col">Published Date</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <?php

            $postShowQuery = "SELECT *  FROM  posts ORDER BY id DESC ";

            $postShowStmt = mysqli_query($dbConnection,$postShowQuery);
            $countAllPost = mysqli_num_rows($postShowStmt);

            if ($countAllPost == 0){

                ?>
                <div class="alert alert-danger">
                    Sorry! No Post Found
                </div>
            <?php
            }else{

                $sl = 1;
                while ($row = mysqli_fetch_array($postShowStmt)){
                    $pid = $row['id'];
                    $title =  $row['title'];
                    $description = $row['description'];
                    $image = $row['image'];
                    $categoryId = $row['category_id'];
                    $authorId = $row['author_id'];
                    $tags = $row['tags'];
                    $status = $row['status'];
                    $postDate = $row['p_date'];

                    $publisherName = "SELECT name FROM users where id = $authorId";
                    $publisherNamestmt = mysqli_query($dbConnection,$publisherName);
                    $publisherNameRow = mysqli_fetch_assoc($publisherNamestmt);
                    $authorName = $publisherNameRow['name'];
                    ?>
                    <tbody class="text-center">

                    <tr class="align-middle">
                        <th><?php echo $sl++; ?></th>
                        <td class=""><?php echo substr($title,0,20); ?></td>
                        <td>
                            <?php
                            if (is_null($image)){
                                ?>
                                <img src="../assets/image/undraw_posting_photo.svg" alt="post thumbnail">
                           <?php
                            }else{
                            ?>
                                <img src="../assets/image/upload/post/<?php echo $image  ; ?>" alt="post thumbnail">
                                    <?php
                            }

                            ?>

                        </td>
                        <td><?php echo $tags ; ?></td>
                        <td><?php echo $status == 1?'Active':'Inactive' ; ?></td>
                        <td><?php echo $authorName ; ?></td>
                        <td><?php echo $postDate; ?></td>
                        <td class="btn-group">
                            <a href="" class="text-decoration-none text-white bg-success  px-2   py-1"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="post.php?do=edit&id=<?php echo $pid; ?>" class=" text-decoration-none text-white bg-primary  px-2 py-1" ><i class="fa fa-edit" aria-hidden="true"></i></a>
                            <a data-toggle="modal" data-target="#deletePost<?php echo $pid; ?>" href="posts.php?do=delete&id=<?php echo $pid; ?>" class=" text-decoration-none text-white bg-danger  py-1 px-2"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>

                    </tr>
                    <!-- Modal -->
                    <div class="modal fade" id="deletePost<?php echo $pid;  ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Are You Sure?
                                </div>
                                <div class="modal-footer">
                                    <a href="#" class="btn btn-primary" data-dismiss="modal">Cancel</a>
                                    <a href="post.php?do=delete&id=<?php echo $pid; ?>" class="btn btn-danger">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>



                    </tbody>
                        <?php
                }
                ?>
            <?php
            }
            ?>
        </table>
    </table>

   <?php }elseif($do == 'add'){


      ?>
       <div class="error_message">
           <?php
           if (!empty($_SESSION['msg'])) {
               echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
               unset($_SESSION['msg']);
           } elseif (!empty($_SESSION['success_msg'])) {
               echo '<div class="alert alert-success">' . $_SESSION['success_msg'] . '</div>';
               unset($_SESSION['success_msg']);
           }
           ?>
       </div>
       <form action="post.php?do=store" method="POST" class="form" id="uploadForm" enctype="multipart/form-data">
           <div class="row">
               <div class="col-md-8">
                   <div class="row">
                       <div class="col-md-12 mt-4">
                           <div class="post-title">
                               <label for="title">Post Title</label>
                               <input type="text" name="title" class="form-control" placeholder="Insert Post Title">
                           </div>
                       </div>
                       <div class="col-md-12 mt-4">
                           <div class="post-description">
                               <label for="description">Description</label>
                               <textarea name="description" id="description"  rows="4" style="resize: none;" class="form-control" placeholder="Type Description"></textarea>
                           </div>
                       </div>

                       <div class="col-md-12 mt-4">
                           <div class="post-tags">
                               <label for="tags">Tags [Separate each tags with comma (,)]</label>
                               <input type="text" name="tags" class="form-control" id="tags" placeholder="Insert Post Tags">
                           </div>
                       </div>
                       <div class="col-md-12 mt-4">
                           <div class="post-status">
                               <label for="post_status">Select Category Status</label>
                               <select name="post_status" id="post_status" class="form-control">
                                   <option value="1">Active</option>
                                   <option value="0">InActive</option>
                               </select>
                           </div>
                       </div>
                       <div class="col-md-12 mt-4">
                           <div class="post-image" >
                               <label for="post_image">Upload Image</label>
                               <input type="file" name="image" id="file" onchange="getImagePreview(event)" class="form-control">

                               <div class="float-right" id="preview"></div>
                           </div>
                       </div>
                       <div class="col-md-12 mt-4">
                           <div class="post-submit">
                               <input type="submit" name="addPost" class="btn btn-primary btn-block">
                           </div>

                       </div>
                   </div>
               </div>
               <div class="col-md-4">
                   <div class="row">
                       <div class="col-md-12 mt-5">
                           <div class="post-category" style="height: auto; overflow-y: scroll; ">
                               <label for="category_id">Select Category</label>

                               <?php
                               $categorySelect = "SELECT * FROM categories WHERE is_parent=0 AND status = 1 ORDER BY cat_title ASC ";
                               $categorySelectStmt = mysqli_query($dbConnection,$categorySelect);
                               while ($row = mysqli_fetch_array($categorySelectStmt)){
                                   $priCategoryId = $row['id'];
                                   $priCategoryName = $row['cat_title'];
                                   ?>
                                   <div class="form-group form-check" >
                                       <input class="form-check-input" type="checkbox" name="category_id[]" id="#category_id" value="<?php echo $priCategoryId?>">

                                       <label class="form-check-label" for="category_id"><?php echo $priCategoryName?></label>

                                   </div>
                                   <?php
                                   $subCategorySelect = "SELECT * FROM categories WHERE is_parent='$priCategoryId' AND status = 1 ORDER BY cat_title ASC ";
                                   $sunCategorySelectStmt = mysqli_query($dbConnection,$subCategorySelect);
                                   while ($row = mysqli_fetch_array($sunCategorySelectStmt)){
                                       $subCategoryId = $row['id'];
                                       $subCategoryName = $row['cat_title'];
                                       ?>
                                       <div class="form-group form-check ml-3" >
                                           <input class="form-check-input" type="checkbox" name="category_id[]" id="#category_id" value="<?php echo $subCategoryId; ?>">

                                           <label class="form-check-label" for="category_id"><?php echo $subCategoryName; ?></label>

                                       </div>
                                       <?php
                                   }
                               }
                               ?>

                           </div>
                       </div>

                   </div>
               </div>
           </div>


       </form>
    <?php

   }elseif ($do == 'store'){
       if (isset($_POST['addPost'])){
           $title = mysqli_real_escape_string($dbConnection,$_POST['title']);
           $description = mysqli_real_escape_string($dbConnection,$_POST['description']);
           $category_id = $_POST['category_id'];
           $allCatId = implode(",",$category_id);
           $author_id = $_SESSION['id'];
           $tags = mysqli_real_escape_string($dbConnection,$_POST['tags']);
           $status = $_POST['post_status'];
           $image = $_FILES['image']['name'];
           $tmpImage = $_FILES['image']['tmp_name'];
           $imageSize = $_FILES['image']['size'];
           $imageType = $_FILES['image']['type'];
           if (!empty($image)) {
               $imageExtention = strtolower(end(explode('.', $image)));
               $defineExtentions = array("jpeg", "jpg", "png");
               if (in_array($imageExtention, $defineExtentions) == true && $imageSize <= 2097152) {
                   $img = rand(1, 999999999) . '-img-' . $image;
                   move_uploaded_file($tmpImage, '../assets/image/upload/post/' . $img);
                    $addPostQuery = "INSERT INTO posts(title,description,image,category_id,author_id,tags,status) VALUES ('$title','$description','$img','$allCatId','$author_id','$tags','$status')";
                   $statement = mysqli_query($dbConnection, $addPostQuery);

                   if ($statement) {
                       $_SESSION['success_msg'] = 'Post  Created Successfully';
                       header('Location:post.php?do=manage');
                   } else {
                       echo 'Failed' . mysqli_error($dbConnection);
                   }
               } else {
                   $_SESSION['msg'] = 'You can only upload Image';
                   header('Location:post.php?do=add');
               }
           } else {
           }
       }


   }elseif ($do == 'edit'){

       if (isset($_GET['id'])){
           $updatePost = $_GET['id'];

           $editPostQuery = "SELECT *  FROM  posts WHERE id = $updatePost";
           $editPostStmt = mysqli_query($dbConnection,$editPostQuery);
           while ($row = mysqli_fetch_array($editPostStmt)) {
               $pid = $row['id'];
               $title = $row['title'];
               $description = $row['description'];
               $image = $row['image'];
               $category_id = $row['category_id'];
               $authorId = $row['author_id'];
               $tags = $row['tags'];
               $status = $row['status'];
               $postDate = $row['p_date'];
               ?>
               <form action="post.php?do=update" method="POST" class="form" id="uploadForm" enctype="multipart/form-data">

                   <div class="row">
                       <div class="col-md-8">
                           <div class="row">
                               <input type="hidden" name="post_id" value="<?php echo $pid; ?>">
                               <div class="col-md-12 ">
                                   <div class="post-title">
                                       <label for="title">Post Title</label>
                                       <input value="<?php echo $title; ?>" type="text"  name="title" class="form-control" placeholder="Insert Post Title">
                                   </div>
                               </div>
                               <div class="col-md-12 mt-4">
                                   <div class="post-description">
                                       <label for="description">Description</label>
                                       <textarea name="description" id="description"  rows="4" style="resize: none;" class="form-control" placeholder="Type Description"><?php echo $description; ?></textarea>
                                   </div>
                               </div>

                               <div class="col-md-12 mt-4">
                                   <div class="post-tags">
                                       <label for="tags">Tags [Separate each tags with comma (,)]</label>
                                       <input value="<?php echo $tags; ?>" type="text" name="tags" class="form-control" id="tags" placeholder="Insert Post Tags">
                                   </div>
                               </div>
                               <div class="col-md-12 mt-4">
                                   <div class="post-status">
                                       <label for="post_status">Select Category Status</label>
                                       <select name="post_status" id="post_status" class="form-control">
                                           <option  <?php echo $status == 1 ? 'selected' : ''; ?> value="1">Active</option>
                                           <option  <?php echo $status == 0 ? 'selected' : ''; ?> value="0">InActive</option>
                                       </select>
                                   </div>
                               </div>
                               <div class="col-md-12 mt-4">
                                   <div class="post-image" >
                                       <label for="post_image">Upload Image</label>
                                       <input type="file" name="image" id="file" onchange="getImagePreview(event)" class="form-control">
                                       <div class="row mt-3 ">
                                           <div class="col-md-6">
                                               <div class="float-end" id="preview"></div>

                                           </div>
                                           <div class="col-md-6">
                                               <div class="perv-img float-right">
                                                   <img style="width: 100px; height: 100px ;" src="../assets/image/upload/post/<?php echo $image; ?>" alt="">
                                               </div>
                                           </div>
                                       </div>


                                   </div>
                               </div>
                               <div class="col-md-12 mt-4">
                                   <div class="post-submit">
                                       <input type="submit" value="Update" name="updatePost" class="btn btn-primary btn-block">
                                   </div>

                               </div>
                           </div>
                       </div>
                       <div class="col-md-4">
                           <div class="row">
                               <div class="col-md-12 mt-4">
                                   <div class="post-category" style="height:auto; overflow-y: scroll; ">
                                       <label for="category_id">Select Category</label>

                                       <?php
                                       $categorySelect = "SELECT * FROM categories WHERE is_parent=0 AND status = 1 ORDER BY cat_title ASC ";
                                       $categorySelectStmt = mysqli_query($dbConnection,$categorySelect);
                                       while ($row = mysqli_fetch_array($categorySelectStmt)){
                                           $priCategoryId = $row['id'];
                                           $priCategoryName = $row['cat_title'];
                                           ?>
                                           <div class="form-group form-check" >
                                               <input class="form-check-input" type="checkbox" name="category_id[]" id="#category_id" value="<?php echo $priCategoryId?>">

                                               <label class="form-check-label" for="category_id"><?php echo $priCategoryName?></label>

                                           </div>
                                           <?php
                                           $subCategorySelect = "SELECT * FROM categories WHERE is_parent='$priCategoryId' AND status = 1 ORDER BY cat_title ASC ";
                                           $sunCategorySelectStmt = mysqli_query($dbConnection,$subCategorySelect);
                                           while ($row = mysqli_fetch_array($sunCategorySelectStmt)){
                                               $subCategoryId = $row['id'];
                                               $subCategoryName = $row['cat_title'];
                                               ?>
                                               <div class="form-group form-check ml-3" >
                                                   <input class="form-check-input" type="checkbox" name="category_id[]" id="#category_id" value="<?php echo $subCategoryId; ?>">

                                                   <label class="form-check-label" for="category_id"><?php echo $subCategoryName; ?></label>

                                               </div>
                                               <?php
                                           }
                                       }
                                       ?>

                                   </div>
                               </div>
                           </div>

                       </div>
                   </div>


               </form>




    <?php
           }
       }


   }elseif ($do == 'update'){

       if (isset($_POST['updatePost'])){
            $pid = $_POST['post_id'];
           $title = mysqli_real_escape_string($dbConnection, $_POST['title']);
           $description = mysqli_real_escape_string($dbConnection, $_POST['description']);
           $category_id = $_POST['category_id'];
           $allCatId = implode(",",$category_id);
           $authorId =  $_SESSION['id'];
           $tags = mysqli_real_escape_string($dbConnection,$_POST['tags']);
           $status =  $_POST['post_status'];
           $image = $_FILES['image']['name'];
           $tmpImage = $_FILES['image']['tmp_name'];
           $imageSize = $_FILES['image']['size'];
           $imageType = $_FILES['image']['type'];
           if (empty($image)){
               $updateQurey = "UPDATE posts SET title='$title',description='$description',category_id='$allCatId',author_id='$authorId',tags='$tags',status='$status' WHERE id=' $pid'";
               $updateStmt = mysqli_query($dbConnection, $updateQurey);
               if ($updateStmt) {
                   $_SESSION['success_msg'] = 'Post Updated Successfully';
                   header('Location:post.php?do=manage');
               } else {
                   die('Not Saved' . mysqli_error($dbConnection));
               }
           }elseif (!empty($image)) {
                   $imageExtention = strtolower(end(explode('.', $image)));
                   $defineExtentions = array("jpeg", "jpg", "png");
                   if (in_array($imageExtention, $defineExtentions) == true && $imageSize <= 2097152) {
                       $query = "SELECT * FROM posts WHERE id='$pid'";
                       $statement = mysqli_query($dbConnection, $query);
                       $item = mysqli_fetch_assoc($statement);
                       unlink('../assets/image/upload/post/' . $item['image']);
                       $imgs = rand(1, 999999999) . '-img-' . $image;
                       move_uploaded_file($tmpImage, '../assets/image/upload/post/' . $imgs);
                     echo  $UpdateQuery = "UPDATE posts SET title = '$title', description = '$description',category_id = '$category_id',author_id = '$authorId',tags = '$tags', status = '$status', image = '$imgs' WHERE id = '$pid'";
                       $updatePost = mysqli_query($dbConnection, $UpdateQuery);

                       if ($updatePost) {
                           header('Location:post.php?do=manage');
                       } else {
                           die('Update Failed' . mysqli_error($dbConnection));
                       }
                   }



           }
       }

   }elseif ($do == 'delete'){
       if (isset($_GET['id'])) {
           $postId = $_GET['id'];
           $postDelQuery = "DELETE FROM posts WHERE id='$postId'";
           $statement = mysqli_query($dbConnection, $postDelQuery);
           if ($statement) {
               $_SESSION['success_msg'] = 'Post Deleted Successfuly';
               header('Location:post.php?do=manage');
           } else {
               echo 'Somethings Went Wrong' . mysqli_error($dbConnection);
           }
       }
   }else{

   }

    ?>
    <div class="container">

    </div>
</div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include('inc/footer.php') ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<?php include('inc/logoutModal.php') ?>

<!-- Bootstrap core JavaScript-->
<?php include('inc/script.php') ?>


<script>
    function getImagePreview(event) {
        var image = URL.createObjectURL(event.target.files[0]);
        var imagediv = document.getElementById('preview');
        var newimg = document.createElement('img');
        imagediv.innerHTML = '';
        newimg.src = image;
        newimg.width = "100";
        newimg.height = "100";
        imagediv.appendChild(newimg);
    }
</script>

<script>
    ClassicEditor
        .create( document.querySelector( '#description' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

</body>

</html>