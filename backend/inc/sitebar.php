<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
        <div class="sidebar-brand-text mx-3">User Management</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="admin.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->


    <?php
    $authId = $_SESSION['id'];
    $query = "SELECT * FROM users WHERE id = '$authId'";
    $statement = mysqli_query($dbConnection, $query);

    while ($row = mysqli_fetch_array($statement)) {
        $authRole = $row['role'];
    }
    if ($authRole == 1) {

    ?>


        <li class="nav-item">
            <a class="nav-link" href="category.php">
                <i class="fas fa-arrow-right"></i> Category
            </a>
        </li>


        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#post" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-users"></i>
                <span> Post</span>
            </a>
            <div id="post" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">All Users</h6>
                    <a class="collapse-item" href="post.php?do=add">Add New Post</a>
                    <a class="collapse-item" href="post.php?do=manage">Manage Posts</a>
                </div>
            </div>
        </li>





        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-users"></i>
                <span> Users</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">All Users</h6>
                    <a class="collapse-item" href="users.php?do=add">Add New Users</a>
                    <a class="collapse-item" href="users.php?do=manage">Manage Users</a>
                </div>
            </div>
        </li>
    <?php
    } else {
    ?>
        <li class="nav-item">
            <a class="nav-link" href="category.php">
                <i class="fas fa-arrow-right"></i> Category
            </a>
        </li>


        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#post" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-users"></i>
                <span> Post</span>
            </a>
            <div id="post" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">All Users</h6>
                    <a class="collapse-item" href="post.php?do=add">Add New Post</a>
                    <a class="collapse-item" href="post.php?do=manage">Manage Posts</a>
                </div>
            </div>
        </li>

        <li class="nav-item ">
            <a class="nav-link logout-btn" href="profile.php?do=profile-view">
                <i class="fa fa-user" aria-hidden="true"></i> Manage Your Profile
            </a>
        </li>
    <?php

    }

    ?>

    <li class="nav-item ">
        <a class="nav-link logout-btn" href="logout.php"><span class="ps-5">Logout</span></a>
    </li>

    <hr class="sidebar-divider">



    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>