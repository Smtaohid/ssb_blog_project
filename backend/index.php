<?php
include('inc/dbConn.php');
ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Management - Login</title>

    <!-- Custom fonts for this template-->
    <link href="../assets/backend/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../assets/backend/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
    <?php
    if (isset($_POST['login'])) {
        if (!empty($_POST['email']) && !empty($_POST['password'])) {
            $email = mysqli_real_escape_string($dbConnection, $_POST['email']);
            $password = mysqli_real_escape_string($dbConnection, $_POST['password']);
            $hasedPass = sha1($password);
            $query = "SELECT * FROM users WHERE email= '$email'  AND  password='$hasedPass' ";
            $statement = mysqli_query($dbConnection, $query);


            $countUser = mysqli_num_rows($statement);

            if ($countUser == 0) {
                $_SESSION['msg'] = "Credential Does Not Match";
            } else {
                while ($row = mysqli_fetch_array($statement)) {
                    $_SESSION['role'] = $row['role'];

                    if ($_SESSION['role']  == 1 || $_SESSION['role']  == 2) {
                        $_SESSION['id'] = $row['id'];
                        $_SESSION['name'] = $row['name'];
                        $_SESSION['email'] = $row['email'];
                        $password = $row['password'];
                        $phone = $row['phone'];
                        $address = $row['address'];
                        $status = $row['status'];
                        $_SESSION['image'] = $row['image'];
                        $join_date = $row['join_date'];

                        if ($email == $_SESSION['email'] && $hasedPass == $password) {
                            header('Location:admin.php');
                        } elseif ($email != $_SESSION['email'] || $password != $hasedPass) {
                            $_SESSION['msg'] = "Credential Does Not Match";
                            header('Location:index.php');
                        }
                    } else {
                        $_SESSION['msg'] = "Sorry You Are Not Admin or Editor";
                        header("refresh:1; url=index.php");
                    }
                }
            }
        } else {
            $_SESSION['msg'] = "Credential Does Not Match";
        }
    }

    ?>
    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                    </div>
                                    <div class="error_message">
                                        <?php
                                        if (!empty($_SESSION['msg'])) {
                                            echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
                                            unset($_SESSION['msg']);
                                        }
                                        ?>
                                    </div>
                                    <form class="user" method="POST">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                                        </div>
                                        <!-- <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div> -->
                                        <input type="submit" value="login" name="login" class="btn btn-primary btn-user btn-block">


                                        <hr>
                                        <a href="#" class="btn btn-google btn-user btn-block">
                                            <i class="fab fa-google fa-fw"></i> Login with Google
                                        </a>
                                        <a href="#" class="btn btn-facebook btn-user btn-block">
                                            <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                                        </a>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="#">Forgot Password?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="register.php">Create an Account!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../assets/backend/vendor/jquery/jquery.min.js"></script>
    <script src="../assets/backend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../assets/backend/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../assets/backend/js/sb-admin-2.min.js"></script>
    <?php
    ob_end_flush();

    ?>
</body>

</html>