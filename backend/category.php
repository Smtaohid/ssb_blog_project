<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include('inc/head.php')
    ?>

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?php include('inc/sitebar.php') ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <?php include('inc/topbar.php') ?>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

           <div class="row">
               <div class="col-md-6">

                   <?php
                   if(isset($_GET['cat_id'])){
                        $catId = $_GET['cat_id'];
                       ?>
                       <!--edit-category-part-start-->
                       <div class="card">
                         <div class="  card-header bg-primary text-white">Edit Category / Sub Category</div>
                           <div class="card-body">
                               <?php
                               $editReadQuery = "SELECT * FROM categories WHERE  id = $catId ORDER BY  order_by ASC ";
                               /** @var TYPE_NAME $dbConnection */
                               $editCatStmt = mysqli_query($dbConnection,$editReadQuery);
                               while ($row = mysqli_fetch_assoc($editCatStmt)) {
                                   $id = $row['id'];
                                   $catTitle = $row['cat_title'];
                                   $catDescription = $row['description'];
                                   $isParent = $row['is_parent'];
                                   $status = $row['status'];
                                   $orderBy = $row['order_by'];

                                   ?>
                                   <div class="error_message">
                                       <?php
                                       if (!empty($_SESSION['msg'])) {
                                           echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
                                           unset($_SESSION['msg']);
                                       }elseif(!empty($_SESSION['success'])){
                                           echo '<div class="alert alert-success">' . $_SESSION['success'] . '</div>';
                                           unset($_SESSION['success']);
                                       }
                                       ?>




                                   </div>
                                   <form action="" method="post" class="form">
                                       <div  class="form-group">
                                           <label for="cat_title">Title</label>
                                           <input value="<?php echo $catTitle ; ?>" type="text"  name="cat_title" id="cat_title" class="form-control" placeholder="Type Category Title" required>
                                       </div>

                                       <div  class="form-group">
                                           <label for="desc">Description</label>
                                           <textarea name="desc" id="desc"  rows="4" style="resize: none;" class="form-control" placeholder="Type Description"><?php echo $catDescription; ?></textarea>
                                       </div>


                                       <div  class="form-group">
                                           <label for="cat_select">Parent Category[Is Any / Optional]</label>
                                           <select name="cat_select" id="cat_select" class="form-control">
                                               <option value="0" selected>Select Category Or SubCategory</option>
                                               <?php
                                               $catSelectQuery = "SELECT * FROM categories WHERE is_parent = 0 ORDER BY order_by ASC ";
                                               $catSelectStmt = mysqli_query($dbConnection,$catSelectQuery);
                                               while ($row = mysqli_fetch_assoc($catSelectStmt)){
                                                   $pId = $row['id'];
                                                   $categoryTitle = $row['cat_title'];
                                                   ?>
                                                   <option value="<?php echo $pId;?>" <?php echo $pId == $isParent?'selected':''; ?> > <?php echo $categoryTitle; ?> </option>

                                                   <?php
                                               }
                                               ?>
                                           </select>
                                       </div>

                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-md-6">
                                                   <label for="cat_status">Select Category Status</label>
                                                   <select name="cat_status" id="cat_status" class="form-control">
                                                       <option value="1" <?php echo $status == 1 ? 'selected' : ''; ?>>Active</option>
                                                       <option value="0" <?php echo $status == 0 ? 'selected' : ''; ?>>InActive</option>
                                                   </select>
                                               </div>
                                               <div class="col-md-6">
                                                   <label for="order_by">Order By</label>
                                                   <input value="<?php echo $orderBy; ?>" type="number" name="order_by" id="order_by" class="form-control" placeholder="Type Order By">
                                               </div>
                                           </div>

                                       </div>
                                       <input type="submit" name="cat_update" id="cat_submit" value="Update" class="btn btn-primary btn-block">
                                   </form>

                            <?php
                               }
                              ?>
                           </div>

                       </div>
                       <!--edit category-part-end-->
                   <?php

                       if (isset($_POST['cat_update'])){
                           $catTitle = $_POST['cat_title'];
                           $cat_select = $_POST['cat_select'];
                           $description = $_POST['desc'];
                           $cat_status = $_POST['cat_status'];
                           $orderBy = $_POST['order_by'];

                           $catUpdateQuery = "UPDATE categories SET cat_title = '$catTitle', is_parent = '$cat_select', description = '$description',status='$cat_status',order_by='$orderBy' WHERE id ='$catId'";
                           $updateStmt = mysqli_query($dbConnection,$catUpdateQuery);
                           if($updateStmt){
                               $_SESSION['success'] = "{$catTitle} Updated Successfully";
                              header('Location:category.php');
                           }else{
                               $_SESSION['msg'] = "{$catTitle} Updated Failed";
                           }
                       }

                   }else{
                       ?>
                       <!--create -category-part-start-->
                       <div class="card">


                           <div class="card-header bg-primary text-white">Add New Category / Sub Category</div>
                           <div class="card-body">

                               <div class="error_message">
                                   <?php
                                   if (!empty($_SESSION['msg'])) {
                                       echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
                                       unset($_SESSION['msg']);
                                   }elseif(!empty($_SESSION['success'])){
                                       echo '<div class="alert alert-success">' . $_SESSION['success'] . '</div>';
                                       unset($_SESSION['success']);
                                   }
                                   ?>




                               </div>
                               <form action="#" method="post">
                                   <div  class="form-group">
                                       <label for="cat_title">Title</label>
                                       <input type="text" name="cat_title" id="cat_title" class="form-control" placeholder="Type Category Title" required>
                                   </div>


                                   <div  class="form-group">
                                       <label for="desc">Description</label>
                                       <textarea name="desc" id="desc"  rows="4" style="resize: none;" class="form-control" placeholder="Type Description"></textarea>
                                   </div>

                                   <div  class="form-group">
                                       <label for="cat_select">Parent Category[Is Any / Optional]</label>
                                       <select name="cat_select" id="cat_select" class="form-control">
                                           <option value="0" selected>Select Category Or SubCategory</option>
                                           <?php
                                           $catSelectQuery = "SELECT * FROM categories  ORDER BY id ASC ";
                                           $catSelectStmt = mysqli_query($dbConnection,$catSelectQuery);
                                           while ($row = mysqli_fetch_assoc($catSelectStmt)){
                                               $catId = $row['id'];
                                               $categoryTitle = $row['cat_title'];
                                               ?>
                                               <option value="<?php echo $catId; ?>" > <?php echo $categoryTitle; ?> </option>

                                               <?php
                                           }
                                           ?>
                                       </select>
                                   </div>

                                   <div class="form-group">
                                       <div class="row">
                                           <div class="col-md-6">
                                               <label for="cat_status">Select Category Status</label>
                                               <select name="cat_status" id="cat_status" class="form-control">
                                                   <option value="1">Active</option>
                                                   <option value="0">InActive</option>
                                               </select>
                                           </div>
                                           <div class="col-md-6">
                                               <label for="order_by">Order By</label>
                                               <input type="number" name="order_by" id="order_by" class="form-control" placeholder="Type Order By">
                                           </div>
                                       </div>

                                   </div>

                                   <input type="submit" name="cat_create" id="cat_submit" value="+ Create" class="btn btn-primary btn-block">

                               </form>
                               <?php

                               if (isset($_POST['cat_create'])){
                                   $catTitle = $_POST['cat_title'];
                                   $cat_select = $_POST['cat_select'];
                                   $description = $_POST['desc'];
                                   $cat_status = $_POST['cat_status'];
                                   $orderBy = $_POST['order_by'];

                                   $query = "INSERT INTO categories(cat_title, is_parent, description, status, order_by)VALUES ('$catTitle','$cat_select','$description','$cat_status','$orderBy')";

                                   $statement = mysqli_query($dbConnection,$query);
                                   if ($statement){
                                       $_SESSION['success'] = "Category Created Successfully";
                                       header('Location:category.php');

                                   }else{
                                       $_SESSION['msg'] = "Please Insert Data Properly";

                                   }
                               }

                               ?>
                           </div>
                       </div>
                       <!--create -category-part-end-->
                   <?php

                   }
                   ?>



               </div>
               <div class="col-md-6 ">
                   <div class="card cat-manage-sec" >
                       <div class="card-header bg-success text-white">Manage All Category</div>


                       <div class="card-body">
                           <table class="table table-bordered">


                               <thead>
                               <tr class="align-middle text-center">
                                   <th style="width: 10px;">SL</th>
                                   <th>Title</th>
                                   <th >Category</th>
                                   <th style="width: 10px;">Status</th>
                                   <th>Action</th>



                               </tr>
                               </thead>

                               <tbody>
                               <?php

                               $readCategory = "SELECT * FROM categories WHERE is_parent = 0 ORDER BY order_by ASC ";
                               $catReadStmt = mysqli_query($dbConnection,$readCategory);
                                   $sl = 1;
                               while ( $row = mysqli_fetch_assoc($catReadStmt)){
                                   $id = $row['id'];
                                   $title = $row['cat_title'];
                                   $parent_cat = $row['is_parent'];
                                   $status = $row['status'];
                                   ?>

                                   <tr class="align-middle text-center">
                                       <td style="width: 10px;"><?php echo $sl++;  ?></td>
                                       <td><?php echo $title;  ?></td>
                                       <td>
                                           <?php 
                                           if ($parent_cat == 0){
                                               ?>
                                               <span class="badge badge-primary">Parent</span>
                                           <?php
                                           }
                                           ?>
                                       </td>
                                       <td style="width: 10px;">
                                           <?php
                                           if ($status ==1){
                                               ?>
                                               <span class="badge badge-success">Active</span>
                                               <?php
                                           }else{
                                               ?>
                                               <span class="badge badge-danger">Inactive</span>
                                           <?php
                                           }
                                           ?>
                                       </td>
                                       <td class="d-flex justify-content-center btn-group">
                                           <a class="btn btn-sm btn-primary" href="">
                                               <i class="fas fa-eye"></i>
                                           </a>
                                           <a href="category.php?cat_id=<?php echo $id; ?>" class="btn btn-sm btn-warning" >
                                               <i class="fas fa-edit"></i>
                                           </a>

                                           <a href="category.php?cat_id=<?php echo $id; ?>" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">
                                               <i class="fas fa-trash"></i>
                                           </a>
                                       </td>


                                       <!-- Button trigger modal -->


                                       <!-- Modal -->
                                       <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                           <div class="modal-dialog">
                                               <div class="modal-content">
                                                   <div class="modal-header">
                                                       <h5 class="modal-title" id="exampleModalLabel">Delete  <?php  echo $title;?> </h5>
                                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                           <span aria-hidden="true">&times;</span>
                                                       </button>
                                                   </div>
                                                   <div class="modal-body">
                                                     Are You Sure?
                                                   </div>
                                                   <div class="modal-footer">
                                                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                       <a href="category.php?del_id=<?php echo $id; ?>" type="button" class="btn btn-danger">Delete</a>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </tr>



                                   <?php
                                   $subCatQuery = "SELECT * FROM categories WHERE is_parent = '$id' ";
                                   $subCat = mysqli_query($dbConnection,$subCatQuery);
                                   while ($row = mysqli_fetch_assoc($subCat)){
                                    $id =  $row['id'];
                                       $title = $row['cat_title'];
                                      $parent_cat = $row['is_parent'];
                                       $status = $row['status'];
                                       ?>

                                       <tr class="align-middle text-center">
                                           <td style="width: 10px;"><?php echo $sl++;  ?></td>
                                           <td> &nbsp;&nbsp;--<?php echo$title;  ?></td>
                                           <td>
                                               <?php
                                               if ($parent_cat == 0){
                                                   ?>
                                                   <span class="badge badge-primary">Parent</span>
                                                   <?php
                                               }
                                               ?>
                                           </td>
                                           <td style="width: 10px;">
                                               <?php
                                               if ($status ==1){
                                                   ?>
                                                   <span class="badge badge-success">Active</span>
                                                   <?php
                                               }else{
                                                   ?>
                                                   <span class="badge badge-danger">Inactive</span>
                                                   <?php
                                               }
                                               ?>
                                           </td>
                                           <td class="d-flex justify-content-center btn-group">
                                               <a class="btn btn-sm btn-primary" href="">
                                                   <i class="fas fa-eye"></i>
                                               </a>
                                               <a href="category.php?cat_id=<?php echo $id; ?>" class="btn btn-sm btn-warning" >
                                                   <i class="fas fa-edit"></i>
                                               </a>
                                               <a href="category.php?subdel_id=<?php echo $id; ?>" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#subdel_id<?php echo $id;?>">
                                                   <i class="fas fa-trash"></i>
                                               </a>
                                           </td>
                                           <!-- Modal -->
                                           <div class="modal fade" id="subdel_id<?php echo $id;?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                               <div class="modal-dialog">
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <h5 class="modal-title" id="subdel_id<?php echo $id;?>">Delete  <?php  echo $title;?> </h5>
                                                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                               <span aria-hidden="true">&times;</span>
                                                           </button>
                                                       </div>
                                                       <div class="modal-body">
                                                           Are You Sure?
                                                       </div>
                                                       <div class="modal-footer">
                                                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                           <a href="category.php?subdel_id=<?php echo $id; ?>" type="button" class="btn btn-danger">Delete</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </tr>




                               <?php

                                       if (isset($_GET['subdel_id'])){
                                           echo  $delId = $_GET['subdel_id'];
                                           $subCatDelQuery = "DELETE FROM categories WHERE is_parent = $id";
                                           $subCatDelStatement = mysqli_query($dbConnection, $subCatDelQuery);
                                           if ($subCatDelStatement){
                                               $_SESSION['success'] = "Category Deleted Successfully";
                                               header('Location:category.php');
                                           }else{
                                               echo "Category Deleted Failed";
                                           }
                                       }

                                   }


                               }
                               ?>
                               </tbody>

                                   </table>
                       </div>
                   </div>
               </div>


               <?php
               if (isset($_GET['del_id'])){
                   $delId = $_GET['del_id'];
                   $catDelQuery = "DELETE FROM categories WHERE id='$delId'";
                   $catDelStatement = mysqli_query($dbConnection, $catDelQuery);
                   if ($catDelStatement){
                       $_SESSION['success'] = "Category Deleted Successfully";
                       header('Location:category.php');
                   }else{
                      echo "Category Deleted Failed";
                   }

               }
               ?>
           </div>


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php include('inc/footer.php') ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<?php include('inc/logoutModal.php') ?>

<!-- Bootstrap core JavaScript-->
<?php include('inc/script.php') ?>
</div>
<script>
    ClassicEditor
        .create( document.querySelector( '#desc' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
</body>

</html>