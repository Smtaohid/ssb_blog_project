<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include('inc/head.php');
    ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include('inc/sitebar.php') ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include('inc/topbar.php') ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->

                <div class="container-fluid">
                    <?php
                    $authId = $_SESSION['id'];
                    $query = "SELECT * FROM users WHERE id = '$authId'";
                    $statement = mysqli_query($dbConnection, $query);

                    while ($row = mysqli_fetch_array($statement)) {
                        $authRole = $row['role'];
                    }
                    if ($authRole == 1) {
                    ?>
                        <!-- Page Heading -->
                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800">Manage All Users</h1>
                        </div>

                        <!-- Content Row -->

                    <?php } ?>


                </div>
                <!-- /.container-fluid -->
                <div class="container">
                    <div class="row">
                        <?php

                        $authId = $_SESSION['id'];
                        $query = "SELECT * FROM users WHERE id = '$authId'";
                        $statement = mysqli_query($dbConnection, $query);

                        while ($row = mysqli_fetch_array($statement)) {
                            $authRole = $row['role'];
                        }
                        if ($authRole == 1) {
                        ?>
                            <div class="col-md-12">
                                <?php
                                if (isset($_GET['do'])) {
                                    $do = $_GET['do'];



                                    if ($do == "manage") {
                                        // echo "We Can Manage All Users Form Database into this page";
                                ?>
                                        <table class="table  table-bordered text-center ">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">SL</th>
                                                    <th scope="col">Photo</th>
                                                    <th scope="col">Name</th>

                                                    <th scope="col">Phone</th>

                                                    <th scope="col">Role</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Join Date</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">

                                                <?php

                                                $qurey = "SELECT * FROM users ORDER BY name ASC";
                                                $allUsers = mysqli_query($dbConnection, $qurey);
                                                $countUsers = mysqli_num_rows($allUsers);
                                                if ($countUsers == 0) { ?>
                                                    <tr>

                                                        <div class="alert alert-danger">
                                                            <p>Sorry!! No Data Found</p>
                                                        </div>
                                                    </tr>

                                                    <?php  } else {
                                                    $sl = 1;
                                                    while ($row = mysqli_fetch_assoc($allUsers)) {
                                                        $id = $row['id'];
                                                        $name = $row['name'];
                                                        $email = $row['email'];
                                                        $phone = $row['phone'];
                                                        $address = $row['address'];
                                                        $role = $row['role'];
                                                        $status = $row['status'];
                                                        $image = $row['image'];
                                                        $join_date = $row['join_date'];



                                                    ?>
                                                        <tr class="align-middle">
                                                            <th scope="row"><?php echo $sl++; ?></th>
                                                            <td class="users">
                                                                <?php if (empty($image)) { ?>
                                                                    </span> <img style="width: 30px;height: 30px;" src="../assets/image/upload/users/60111.jpg" alt="">

                                                                <?php } else { ?>

                                                                    <img style="height: 30px; width: 40px;" src="../assets/image/upload/users/<?php echo $image; ?>" alt="<?php echo $name; ?>">

                                                                <?php } ?>
                                                            </td>
                                                            <td><?php echo $name; ?></td>

                                                            <td><?php echo $phone ?></td>

                                                            <td>
                                                                <?php
                                                                if ($role == 1) {


                                                                ?>
                                                                    <span class="badge badge-success">Admin</span>
                                                                <?php } elseif ($role == 2) { ?>
                                                                    <span class="badge badge-primary">Editor</span>
                                                                <?php  } elseif ($role == 3) { ?>
                                                                    <span class="badge badge-warning">User</span>
                                                                <?php } else { ?>
                                                                    <span class="bagde badge-danger">Unidentified</span>
                                                                <?php } ?>
                                                            </td>
                                                            <td><?php echo $status == 1 ? 'Active' : 'Block' ?></td>
                                                            <td><?php echo $join_date; ?></td>
                                                            <td>
                                                                <a href="users.php?do=show&id=<?php echo $id; ?>" class="text-decoration-none text-white bg-success rounded px-2 py-1"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                <a href="users.php?do=edit&id=<?php echo $id; ?>" class=" text-decoration-none text-white bg-primary rounded px-2 py-1" href=""><i class="fa fa-edit" aria-hidden="true"></i></a>
                                                                <a data-toggle="modal" data-target="#deletephp<?php echo $id ?>" href="users.php?do=delete&id=<?php echo $id; ?>" class=" text-decoration-none text-white bg-danger rounded py-1 px-2"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            </td>

                                                        </tr>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="deletephp<?php echo $id ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Delete <?php echo $name ?></h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Are You Sure?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <a href="#" class="btn btn-primary" data-dismiss="modal">Cancel</a>
                                                                        <a href="users.php?do=delete&id=<?php echo $id; ?>" class="btn btn-danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                <?php
                                                    }
                                                } ?>
                                            </tbody>
                                        </table>
                                    <?php
                                    } elseif ($do == 'add') {

                                    ?>
                                        <div class="error_message">
                                            <?php
                                            if (!empty($_SESSION['msg'])) {
                                                echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
                                                unset($_SESSION['msg']);
                                            } elseif (!empty($_SESSION['success_msg'])) {
                                                echo '<div class="alert alert-success">' . $_SESSION['success_msg'] . '</div>';
                                                unset($_SESSION['success_msg']);
                                            }
                                            ?>
                                        </div>
                                        <form action="users.php?do=store" method="post" class="form" id="uploadForm" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="name"> Full Name</label>
                                                    <input type="text" name="name" id="name" required class="form-control" placeholder="Insert Your Name">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="email"> Email</label>
                                                    <input type="email" name="email" id="email" required class="form-control" placeholder="Insert Your Email">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="phone"> Phone </label>
                                                    <input type="text" name="phone" id="phone" required class="form-control" placeholder="Insert Your Phone">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="mt-3" for="password">Password</label>
                                                    <input required type="password" name="password" id="password" class="form-control " placeholder="Insert Your Password">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="mt-3" for="conf-password"> Confirm Password</label>
                                                    <input required type="password" name="conf_password" id="conf-password" class="form-control " placeholder="Insert Your Password">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="mt-3" for="role"> Select Role</label>
                                                    <select name="role" id="role" class="form-control">

                                                        <option value="3">Select Role</option>
                                                        <option value="1">Admin</option>
                                                        <option value="2">Editor</option>
                                                        <option value="3">User</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12 ">
                                                    <label class="mt-3" for="address"> Address </label>
                                                    <textarea style="resize: none!important;" class="inp-textarea form-control" name="address" id="address" rows="1"></textarea>
                                                </div>
                                                <div class="col-md-6 mt-4">
                                                    <label class="" for="status"> Select Status</label>
                                                    <select name="status" id="status" class="form-control">

                                                        <option value="1">Select Status</option>
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>

                                                    </select>
                                                </div>
                                                <div class="row mx-auto mt-3">
                                                    <div class="col-md-6">
                                                        <label for="file" class=" mt-3">Isert Profile Image</label><br>

                                                        <input type="file" name="image" id="file" onchange="getImagePreview(event)">

                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="float-right" id="preview"></div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <input type="submit" name="submit" class="btn btn-primary btn-block mt-3">

                                                </div>
                                            </div>
                                        </form>

                                    <?php
                                    } elseif ($do == 'store') {

                                        if (isset($_POST['submit'])) {
                                            $name = mysqli_real_escape_string($dbConnection, $_POST['name']);
                                            $email = mysqli_real_escape_string($dbConnection, $_POST['email']);
                                            $phone = mysqli_real_escape_string($dbConnection, $_POST['phone']);
                                            $password = mysqli_real_escape_string($dbConnection, $_POST['password']);
                                            $conf_password = mysqli_real_escape_string($dbConnection, $_POST['conf_password']);
                                            $role = $_POST['role'];
                                            $address = mysqli_real_escape_string($dbConnection, $_POST['address']);
                                            $status = $_POST['status'];
                                            $image = $_FILES['image']['name'];
                                            $tmpImage = $_FILES['image']['tmp_name'];
                                            $imageSize = $_FILES['image']['size'];
                                            $imageType = $_FILES['image']['type'];

                                            if ($password == $conf_password) {
                                                if (strlen($password) <= 7 || strlen($password) > 100) {
                                                    $_SESSION['msg'] = "Password must be at least 8 characters";
                                                    header("refresh:.001; url=users.php?do=add");
                                                } else {
                                                    $hassedPass = sha1($password);
                                                    if (!empty($image)) {
                                                        $imageExtention = strtolower(end(explode('.', $image)));
                                                        $defineExtentions = array("jpeg", "jpg", "png", "webp");
                                                        if (in_array($imageExtention, $defineExtentions) == true && $imageSize <= 2097152) {
                                                            $img = rand(1, 999999999) . '-img-' . $image;

                                                            move_uploaded_file($tmpImage, '../assets/image/upload/users/' . $img);
                                                            $qurey = "INSERT INTO users (name,email,password,phone,address,role,status,image,join_date) VALUES('$name','$email','$hassedPass','$phone','$address','$role','$status','$img', now())";
                                                            $statement = mysqli_query($dbConnection, $qurey);

                                                            if ($statement) {
                                                                header('Location:users.php?do=manage');
                                                            } else {
                                                                echo 'Failed' . mysqli_error($dbConnection);
                                                            }
                                                        } else {
                                                            $_SESSION['msg'] = 'You can only upload Image';
                                                            header('Location:users.php?do=add');
                                                        }
                                                    } else {
                                                    }
                                                }
                                            } else {
                                                $_SESSION['msg'] = "Password Doesn't Match";
                                                header("refresh:.001; url=users.php?do=add");
                                            }
                                        }

                                        // echo 'all create operation run by this page';
                                    } elseif ($do == 'show') {
                                        if (isset($_GET['id'])) {
                                            $userId = $_GET['id'];
                                            $qurey = "SELECT * FROM users WHERE id=$userId";
                                            $user = mysqli_query($dbConnection, $qurey);
                                            $row = mysqli_fetch_assoc($user);
                                        }
                                    ?>
                                        <div class="row">
                                            <div class="col-md-10 mx-auto">
                                                <div class="card">
                                                    <div class="card-header bg-primary text-white">
                                                        <h4>User Details</h4>
                                                    </div>
                                                    <?php

                                                    if ($row['image']) {
                                                    ?>
                                                        <img style="width:300px ; height:300px ;" class="card-img-top mx-auto mt-3 img-thumbnail" src="../assets/image/upload/users/<?php echo $row['image']; ?>" alt="<?php echo $row['name']; ?>">

                                                    <?php
                                                    }
                                                    ?>
                                                    <div class="card-body">
                                                        <table class="table table-striped table-bordered text-center">
                                                            <tr>
                                                                <th>Name</th>
                                                                <td><?php echo $row['name']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Email</th>
                                                                <td><?php echo $row['email']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Phone</th>
                                                                <td><?php echo $row['phone']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Adedress</th>
                                                                <td><?php echo $row['address']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Role</th>
                                                                <td><?php
                                                                    if ($row['role'] == 1) {
                                                                    ?>
                                                                        <span class="badge badge-success">Admin</span>
                                                                    <?php
                                                                    } elseif ($row['role'] == 2) {

                                                                    ?>
                                                                        <span class="badge badge-primary">Editor</span>
                                                                    <?php
                                                                    } elseif ($row['role'] == 3) {
                                                                    ?>

                                                                        <span class="badge badge-warning">User</span>
                                                                    <?php
                                                                    } else {
                                                                    ?>
                                                                        <span class="badge badge-danger">Unidentifide</span>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Status</th>
                                                                <td>
                                                                    <?php if ($row['status'] == 1) {
                                                                    ?>
                                                                        <span class="badge badge-success">Active</span>
                                                                    <?php
                                                                    } else {
                                                                    ?>

                                                                        <span class="badge badge-danger">Inactive</span>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Join Date</th>
                                                                <td><?php echo $row['join_date'] ?></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <a href="users.php?do=manage" class=" card-btn btn btn-primary">Back</a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php
                                    } elseif ($do == 'edit') {
                                        if (isset($_GET['id'])) {
                                            $userId = $_GET['id'];
                                            $qurey = "SELECT * FROM users WHERE id=$userId";
                                            $user = mysqli_query($dbConnection, $qurey);
                                            $row = mysqli_fetch_assoc($user);
                                        }

                                        echo '  Users can edit data  and this is   html form  templeate';
                                    ?>
                                        <form action="users.php?do=update" method="POST" class="form" enctype="multipart/form-data">
                                            <input type="hidden" name="user_id" value="<?php echo $row['id'] ?>">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="name"> Full Name</label>
                                                    <input type="text" name="name" value="<?php echo $row['name'] ?>" id="name" required class="form-control" placeholder="Insert Your Name">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="email"> Email</label>
                                                    <input type="email" value="<?php echo $row['email'] ?>" name="email" id="email" required class="form-control" placeholder="Insert Your Email">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="phone"> Phone </label>
                                                    <input type="text" value="<?php echo $row['phone'] ?>" name="phone" id="phone" required class="form-control" placeholder="Insert Your Phone">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="mt-3" for="password">Password</label>
                                                    <input type="password" name="password" id="password" class="form-control " placeholder="********">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="mt-3" for="conf-password"> Confirm Password</label>
                                                    <input type="password" name="conf_password" id="conf-password" class="form-control " placeholder="Re-Type Your Password">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="mt-3" for="role"> Select Role</label>
                                                    <select name="role" id="role" class="form-control">

                                                        <option value="3">Select Role</option>
                                                        <option <?php echo $row['role'] == 1 ? 'selected' : null; ?> value="1">Admin</option>
                                                        <option <?php echo $row['role'] == 2 ? 'selected' : null; ?> value="2">Editor</option>
                                                        <option <?php echo $row['role'] == 3 ? 'selected' : null; ?> value="3">User</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12 ">
                                                    <label class="mt-3" for="address"> Address </label>
                                                    <textarea style="resize: none!important;" class="inp-textarea form-control" name="address" id="address" rows="1"><?php echo $row['address'] ?></textarea>
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="mt-3" for="status"> Select Status</label>
                                                    <select name="status" id="status" class="form-control">

                                                        <option value="1">Select Status</option>
                                                        <option <?php echo $row['status'] == 1 ? 'selected' : null; ?> value="1">Active</option>
                                                        <option <?php echo $row['status'] == 0 ? 'selected' : null; ?> value="0">Inactive</option>

                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row mt-4 text-center mx-auto">
                                                        <div class="col-md-6">
                                                            <p>Inssert Photo: </p>
                                                            <div class="mt-3" id="preview"></div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php if (empty($row['image'])) { ?>
                                                                </span> <img style="width: 100px;height: 100px;" src="../assets/image/upload/users/60111.jpg" alt="">

                                                            <?php } else { ?>
                                                                <p>Prev. Photo:</p>
                                                                <img style="width: 100px;height: 100px;" src="../assets/image/upload/users/<?php echo $row['image']; ?>" alt="">
                                                            <?php } ?>
                                                        </div>
                                                    </div>



                                                    <label for="image" class=" mt-3">Isert Profile Image </label>

                                                    <input type="file" name="image" id="file" onchange="getImagePreview(event)">
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="submit" name="submit" value="Save Changes" class="btn btn-primary btn-block mt-3">
                                                </div>
                                            </div>
                                        </form>

                                <?php

                                    } elseif ($do == 'update') {
                                        if (isset($_POST['submit'])) {
                                            $userId = $_POST['user_id'];
                                            $name = $_POST['name'];
                                            $email = $_POST['email'];
                                            $phone = $_POST['phone'];
                                            $password = $_POST['password'];
                                            $conf_password = $_POST['conf_password'];
                                            $role = $_POST['role'];
                                            $address = $_POST['address'];
                                            $status = $_POST['status'];
                                            $image = $_FILES['image']['name'];
                                            $tmpImage = $_FILES['image']['tmp_name'];

                                            if (empty($password) && empty($image)) {
                                                $qurey = "UPDATE users SET name='$name',email='$email',phone='$phone',address='$address',role='$role',status='$status' WHERE id='$userId'";
                                                $updateStmt = mysqli_query($dbConnection, $qurey);
                                                if ($updateStmt) {
                                                    header('Location:users.php?do=manage');
                                                } else {
                                                    die('Not Saved' . mysqli_error($dbConnection));
                                                }
                                            } elseif (!empty($password) && !empty($image)) {
                                                if ($password == $conf_password) {
                                                    $hassedPass = sha1($password);
                                                }
                                                $query = "SELECT * FROM users WHERE id='$userId'";
                                                $statement = mysqli_query($dbConnection, $query);
                                                $item = mysqli_fetch_assoc($statement);
                                                unlink('../assets/image/upload/users/' . $item['image']);
                                                $imgs = rand(1, 999999999) . '-img-' . $image;
                                                move_uploaded_file($tmpImage, '../assets/image/upload/users/' . $imgs);
                                                $UpdateQuery = "UPDATE users SET name='$name',email='$email',password='$hassedPass',phone='$phone',address='$address',role='$role',status='$status',image='$imgs' WHERE id='$userId'";
                                                $updateUser = mysqli_query($dbConnection, $UpdateQuery);

                                                if ($updateUser) {
                                                    header('Location:users.php?do=manage');
                                                } else {
                                                    die('Update Failed' . mysqli_error($dbConnection));
                                                }
                                            } elseif (empty($password) && !empty($image)) {

                                                $query = "SELECT * FROM users WHERE id='$userId'";
                                                $statement = mysqli_query($dbConnection, $query);
                                                $item = mysqli_fetch_assoc($statement);
                                                unlink('../assets/image/upload/users/' . $item['image']);
                                                $imgs = rand(1, 999999999) . '-img-' . $image;
                                                move_uploaded_file($tmpImage, '../assets/image/upload/users/' . $imgs);
                                                $UpdateQuery = "UPDATE users SET name='$name',email='$email',phone='$phone',address='$address',role='$role',status='$status',image='$imgs' WHERE id='$userId'";
                                                $updateUser = mysqli_query($dbConnection, $UpdateQuery);

                                                if ($updateUser) {
                                                    header('Location:users.php?do=manage');
                                                } else {
                                                    die('Update Failed' . mysqli_error($dbConnection));
                                                }
                                            } elseif (!empty($password) && empty($image)) {
                                                if ($password == $conf_password) {
                                                    $hassedPass = sha1($password);
                                                }
                                                /** @var TYPE_NAME $hassedPass */
                                                $UpdateQuery = "UPDATE users SET name='$name',email='$email',password='$hassedPass',phone='$phone',address='$address',role='$role',status='$status'WHERE id='$userId'";
                                                $updateUser = mysqli_query($dbConnection, $UpdateQuery);

                                                if ($updateUser) {
                                                    header('Location:users.php?do=manage');
                                                } else {
                                                    die('Update Failed' . mysqli_error($dbConnection));
                                                }
                                            }
                                        }
                                    } elseif ($do == 'delete') {
                                        if (isset($_GET['id'])) {
                                            $userId = $_GET['id'];
                                            $query = "DELETE FROM users WHERE id='$userId'";
                                            $statement = mysqli_query($dbConnection, $query);
                                            if ($statement) {
                                                header('Location:users.php?do=manage');
                                            } else {
                                                echo 'Somethings Went Wrong' . mysqli_error($dbConnection);
                                            }
                                        }
                                    }
                                } else {
                                    echo 'Page Can not Found';
                                }



                                ?>
                            </div>

                        <?php

                        } else {

                        ?>


                            <div class="col-md-12 mx-auto">
                                <div class=" alert alert-danger">
                                    Sorry!! Your Are Not Allow This Page
                                </div>
                            </div>

                        <?php
                        }
                        ?>

                    </div>
                </div>

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php include('inc/footer.php') ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <img src="" alt="">
    <!-- Logout Modal-->
    <?php include('inc/logoutModal.php') ?>

    <!-- Bootstrap core JavaScript-->
    <?php include('inc/script.php') ?>
    <script>
        function getImagePreview(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('preview');
            var newimg = document.createElement('img');
            imagediv.innerHTML = '';
            newimg.src = image;
            newimg.width = "100";
            newimg.height = "100";
            imagediv.appendChild(newimg);
        }
    </script>
</body>

</html>