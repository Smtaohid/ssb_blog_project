

<!doctype html>
<html lang="en">
<head>
    <?php
    include "includes/header.php";
    ?>
</head>
<body>
<!-- :::::::::: Header Section Start :::::::: -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
               <?php include "includes/navbar.php"; ?>
            </div>
        </div>
    </div>
</header>
<!-- ::::::::::: Header Section End ::::::::: -->
<!-- :::::::::: Page Banner Section Start :::::::: -->
<section class="blog-bg background-img">
    <div class="container">
        <!-- Row Start -->
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Search Page</h2>
                <!-- Page Heading Breadcrumb Start -->
                <nav class="page-breadcrumb-item">
                    <ol>
                        <li><a href="search.php">Search <i class="fa fa-angle-double-right"></i></a></li>
                        <!-- Active Breadcrumb -->
                        <li class="active">Blog</li>
                    </ol>
                </nav>
                <!-- Page Heading Breadcrumb End -->
            </div>

        </div>
        <!-- Row End -->
    </div>
</section>
<!-- ::::::::::: Page Banner Section End ::::::::: -->



<!-- :::::::::: Blog With Right Sidebar Start :::::::: -->
<section>
    <div class="container">
        <div class="row">
            <!-- Blog Posts Start -->
            <div class="col-md-8">

                <?php
                if (isset($_GET['s'])){
                    $searchContent = $_GET['s'];
                    $searchContentQuery = "SELECT * FROM posts WHERE title LIKE '%$searchContent%' OR description LIKE '%$searchContent%' OR tags LIKE '%$searchContent%' ORDER BY id DESC ";
                    /** @var TYPE_NAME $dbConnection */
                    $searchContentStmt = mysqli_query($dbConnection,$searchContentQuery);

                    $numberOfSearchPosts = mysqli_num_rows($searchContentStmt);

                    if ($numberOfSearchPosts == 0){
                        ?>
                        <div class="alert alert-danger">Sorry!! No Data Found</div>
                <?php
                    }else{
                        while ($row = mysqli_fetch_assoc($searchContentStmt)){
                            $id = $row['id'];
                            $title = $row['title'];
                            $description = $row['description'];
                            $image = $row['image'];
                            $category_id = $row['category_id'];
                            $authorId = $row['author_id'];
                            $tags = $row['tags'];
                            $status = $row['status'];
                            $postDate = $row['p_date'];
                            $changeDate = date("D-M-Y", strtotime($postDate));

                            $categoryReadQuery = "SELECT cat_title FROM categories WHERE status =1 AND id ='$category_id'";
                            $categoryReadStmt = mysqli_query($dbConnection,$categoryReadQuery);
                            $categoryTitle = mysqli_fetch_assoc($categoryReadStmt);

                            ?>


                            <!-- Single Item Blog Post Start -->
                            <div class="blog-post">
                                <!-- Blog Banner Image -->
                                <div class="blog-banner">
                                    <a href="  singlePage.php?spid=<?php echo $id;?>">
                                        <?php
                                        if (!empty($image)){
                                            ?>
                                            <img src="assets/image/upload/post/<?php echo $image; ?>" >
                                            <?php
                                        }else{
                                            ?>
                                            <img src="assets/image/undraw_posting_photo.svg ?>" >
                                            <?php
                                        }
                                        ?>

                                        <!-- Post Category Names -->
                                        <div class="blog-category-name">
                                            <h6>
                                                <?php
                                                $allCat = explode(",",$category_id);
                                                foreach ($allCat as $catId){
                                                    $catReadQuery  = "SELECT * FROM categories WHERE id = '$catId'";
                                                    $catReadStmt = mysqli_query($dbConnection,$catReadQuery);
                                                    while ($catRow = mysqli_fetch_assoc($catReadStmt)){
                                                        $catId = $catRow['id'];
                                                        $catTitle = $catRow['cat_title'];
                                                        ?>
                                                        <span class="ml-1"><a class=" btn btn-sm btn-main" href="category.php?catid=<?php echo $catId;?>"><?php echo $catTitle; ?></a></span>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </h6>
                                        </div>
                                    </a>
                                </div>
                                <!-- Blog Title and Description -->
                                <div class="blog-description">
                                    <a href="singlePage.php?spid=<?php echo $id;?>">
                                        <h3 class="post-title"><?php echo $title; ?></h3>
                                    </a>
                                    <p><?php echo substr( $description,0,200); ?></p>


                                    <!-- Blog Info, Date and Author -->
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="blog-info">
                                                <ul>
                                                    <li><i class="fa fa-calendar"></i><?php echo  $changeDate; ?></li>
                                                    <li><i class="fa fa-user"></i>by - admin</li>
                                                    <li><i class="fa fa-heart"></i>(50)</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col-md-4 read-more-btn">
                                            <button type="button" class="btn-main">
                                                <a href="singlePage.php?spid=<?php echo $id;?>">Read More <i class="fa fa-angle-double-right"></i></a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Item Blog Post End -->
                            <?php
                        }

                    }
                }
                ?>


                <?php
                if (isset($_POST['search'])){
                    /** @var TYPE_NAME $dbConnection */
                    $searchContent = mysqli_real_escape_string($dbConnection, $_POST['search']);
                    $searchContentQuery = "SELECT * FROM posts WHERE title LIKE '%$searchContent%' OR description LIKE '%$searchContent%' OR tags LIKE '%$searchContent%' ORDER BY id DESC ";

                    $searchContentStmt = mysqli_query($dbConnection,$searchContentQuery);

                    $numberOfSearchPosts = mysqli_num_rows($searchContentStmt);

                    if ($numberOfSearchPosts == 0){
                        ?>
                        <div class="alert alert-danger">Sorry!! No Data Found</div>
                        <?php
                    }else{


                        while ($row = mysqli_fetch_assoc($searchContentStmt)){
                           $id = $row['id'];
                            $title = $row['title'];
                            $description = $row['description'];
                            $image = $row['image'];
                            $category_id = $row['category_id'];
                            $authorId = $row['author_id'];
                            $tags = $row['tags'];
                            $status = $row['status'];
                            $postDate = $row['p_date'];
                            $changeDate = date("D-M-Y", strtotime($postDate));
                            $categoryReadQuery = "SELECT cat_title FROM categories WHERE status =1 AND id ='$category_id'";
                            $categoryReadStmt = mysqli_query($dbConnection,$categoryReadQuery);
                            $categoryTitle = mysqli_fetch_assoc($categoryReadStmt);

                            ?>


                            <!-- Single Item Blog Post Start -->
                            <div class="blog-post">
                                <!-- Blog Banner Image -->
                                <div class="blog-banner">
                                    <a href="  singlePage.php?spid=<?php echo $id;?>">
                                        <?php
                                        if (!empty($image)){
                                            ?>
                                            <img src="assets/image/upload/post/<?php echo $image; ?>" >
                                            <?php
                                        }else{
                                            ?>
                                            <img src="assets/image/undraw_posting_photo.svg ?>" >
                                            <?php
                                        }
                                        ?>

                                        <!-- Post Category Names -->
                                        <div class="blog-category-name">
                                            <h6>
                                                <?php
                                                $allCat = explode(",",$category_id);
                                                foreach ($allCat as $catId){
                                                    $catReadQuery  = "SELECT * FROM categories WHERE id = '$catId'";
                                                    $catReadStmt = mysqli_query($dbConnection,$catReadQuery);
                                                    while ($catRow = mysqli_fetch_assoc($catReadStmt)){
                                                        $catId = $catRow['id'];
                                                        $catTitle = $catRow['cat_title'];
                                                        ?>
                                                        <span class="ml-1"><a class=" btn btn-sm btn-main" href="category.php?catid=<?php echo $catId;?>"><?php echo $catTitle; ?></a></span>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </h6>
                                        </div>
                                    </a>
                                </div>
                                <!-- Blog Title and Description -->
                                <div class="blog-description">
                                    <a href="  singlePage.php?spid=<?php echo $id;?>">
                                        <h3 class="post-title"><?php echo $title; ?></h3>
                                    </a>
                                    <p><?php echo substr($description,0,300); ?></p>


                                    <!-- Blog Info, Date and Author -->
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="blog-info">
                                                <ul>
                                                    <li><i class="fa fa-calendar"></i><?php echo  $changeDate; ?></li>
                                                    <li><i class="fa fa-user"></i>by - admin</li>
                                                    <li><i class="fa fa-heart"></i>(50)</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col-md-4 read-more-btn">
                                            <button type="button" class="btn-main">
                                                <a href="singlePage.php?spid=<?php echo $id; ?>">Read More <i class="fa fa-angle-double-right"></i></a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Item Blog Post End -->
                            <?php
                        }

                    }
                }
                ?>
            </div>


            <?php
            include "includes/sidebar.php";
            ?>

        </div>
    </div>
</section>
<!-- ::::::::::: Blog With Right Sidebar End ::::::::: -->




<!-- :::::::::: Footer Section Start :::::::: -->
<footer>
    <?php
    include "includes/footer.php";
    ?>
</footer>
<!-- ::::::::::: Footer Section End ::::::::: -->

<?php include 'includes/script.php'; ?>
</body>
</html>
