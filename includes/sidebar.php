<!-- Blog Right Sidebar -->
                <div class="col-md-4">

                    <!-- Latest News -->
                    <div class="widget">
                        <h4>Latest News</h4>
                        <div class="title-border"></div>
                        
                        <!-- Sidebar Latest News Slider Start -->
                        <div class="sidebar-latest-news owl-carousel owl-theme">


                            <?php
                            $postQuery = "SELECT * FROM posts  WHERE status = 1 ORDER BY id DESC LIMIT 3 ";

                            /** @var TYPE_NAME $dbConnection */
                            $postQueryStmt = mysqli_query($dbConnection,$postQuery);
                            $numberOfposts = mysqli_num_rows($postQueryStmt);

                                while ($row = mysqli_fetch_assoc($postQueryStmt)){
                                    $pId = $row['id'];
                                    $title = $row['title'];
                                    $description = $row['description'];
                                    $image = $row['image'];
                                    $category_id = $row['category_id'];
                                    $authorId = $row['author_id'];
                                    $tags = $row['tags'];
                                    $status = $row['status'];
                                    $postDate = $row['p_date'];
                                    $changeDate = date("D-M-Y", strtotime($postDate));
                                    ?>
                                    <!-- Single Item Blog Post Start -->
                                    <div class="item">
                                        <div class="latest-news">
                                            <!-- Latest News Slider Image -->
                                            <div class="latest-news-image">
                                                <a href="singlePage.php?spid=<?php echo $pId;  ?>">
                                                    <img src="assets/image/upload/post/<?php echo $image; ?>">
                                                </a>
                                            </div>
                                            <!-- Latest News Slider Heading -->
                                            <h5><?php echo $title; ?></h5>
                                            <!-- Latest News Slider Paragraph -->
                                            <p><?php echo substr($description,0,220); ?></p>
                                        </div>
                                    </div>
                                    <!-- Single Item Blog Post End -->
                                    <?php
                                }
                            ?>
                            <!-- First Latest News Start -->

                            <!-- First Latest News End -->
                            

                        </div>
                        <!-- Sidebar Latest News Slider End -->
                    </div>


                    <!-- Search Bar Start -->
                    <div class="widget"> 
                            <!-- Search Bar -->
                            <h4>Blog Search</h4>
                            <div class="title-border"></div>
                            <div class="search-bar">
                                <!-- Search Form Start -->
                                <form action="search.php" method="POST">
                                    <div class="form-group">
                                        <input type="text" name="search" placeholder="Search Here" autocomplete="off" class="form-input">
                                        <i class="fa fa-paper-plane-o"></i>
                                    </div>
                                </form>
                                <!-- Search Form End -->
                            </div>
                    </div>
                    <!-- Search Bar End -->

                    <!-- Recent Post -->
                    <div class="widget">
                        <h4>Recent Posts</h4>
                        <div class="title-border"></div>
                        <div class="recent-post">
                            <?php
                            $postQuery = "SELECT * FROM posts  WHERE status = 1 ORDER BY p_date ASC LIMIT 4 ";

                            $postQueryStmt = mysqli_query($dbConnection,$postQuery);
                            $numberOfposts = mysqli_num_rows($postQueryStmt);

                            while ($row = mysqli_fetch_assoc($postQueryStmt)){
                                 $pId = $row['id'];
                                $title = $row['title'];
                                $description = $row['description'];
                                $image = $row['image'];
                                $category_id = $row['category_id'];
                                $authorId = $row['author_id'];
                                $tags = $row['tags'];
                                $status = $row['status'];
                                $postDate = $row['p_date'];
                                $changeDate = date("D-M-Y", strtotime($postDate));
                                ?>
                                <!-- Recent Post Item Content Start -->

                                <div class="recent-post-item">

                                    <div class="row">
                                        <!-- Item Image -->
                                        <a href="singlePage.php?spid=<?php echo $pId; ?>">
                                            <div class="col-md-4">
                                                <img src="assets/image/upload/post/<?php echo $image; ?>">
                                            </div>
                                        </a>

                                        <!-- Item Tite -->
                                        <div class="col-md-8 no-padding">
                                            <h5><?php echo $title; ?></h5>
                                            <ul>
                                                <li><i class="fa fa-clock-o"></i><?php echo $changeDate; ?></li>
                                                <li><i class="fa fa-comment-o"></i>15</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- Recent Post Item Content End -->

                                <?php
                            }
                            ?>

                        </div>
                    </div>

                    <!-- All Category -->
                    <div class="widget">
                        <h4>Blog Categories</h4>
                        <div class="title-border"></div>
                        <!-- Blog Category Start -->
                        <div class="blog-categories">
                            <ul>

                                <?php
                                $categoryQuery = "SELECT * FROM categories WHERE  status = 1 ORDER BY cat_title DESC ";
                             $categoryStmt = mysqli_query($dbConnection,$categoryQuery);
                             while ($row = mysqli_fetch_assoc($categoryStmt)){

                                 $catId = $row['id'];
                                 $totalPostsQuery = "SELECT * FROM posts WHERE status = 1 AND  category_id = '$catId'";
                                 $totalPostsStmt = mysqli_query($dbConnection,$totalPostsQuery);
                                 $totalPosts = mysqli_num_rows($totalPostsStmt);



                                ?>
                                <!-- Category Item -->
                                 <a href="category.php?catid=<?php echo $catId; ?>">
                                     <li>

                                         <i class="fa fa-check"></i>
                                         <?php echo $row['cat_title']; ?>


                                         <span><?php echo $totalPosts; ?></span>
                                     </li>
                                 </a>


                                <?php  }?>
                                <!-- Category Item -->

                            </ul>
                        </div>
                        <!-- Blog Category End -->
                    </div>

                    <!-- Recent Comments -->
                    <div class="widget">
                        <h4>Recent Comments</h4>
                        <div class="title-border"></div>
                        <div class="recent-comments">

                            <?php
                            /** @var TYPE_NAME $pId */
                            $showCommentQuery = "SELECT * FROM comments WHERE status = 1 ORDER BY comment_date DESC LIMIT 3 ";
                            $showCommentStmt = mysqli_query($dbConnection,$showCommentQuery);
                            $totalComments = mysqli_num_rows($showCommentStmt);
                            if ($totalComments == 0){

                            ?>
                                <div class="alert alert-warning">
                                    No Comments Found!
                                </div>
                                <?php
                            }else{
                            while ($row = mysqli_fetch_assoc($showCommentStmt)){
                            $commentsId = $row['id'];
                            $postId = $row['post_id'];
                            $userId = $row['user_id'];
                            $comment = $row['comments'];
                            $status = $row['status'];
                            $commentDate = $row['comment_date'];
                            $changeDate = date("D-M-Y", strtotime($commentDate));


                            $commentUserRead = "SELECT * FROM users WHERE id = '$userId' AND status = 1";
                            $commentUserStmt = mysqli_query($dbConnection,$commentUserRead);
                            while ($userRow = mysqli_fetch_assoc($commentUserStmt)){
                            $userName = $userRow['name'];
                            $userImage = $userRow['image'];
                            ?>
                            <!-- Recent Comments Item Start -->
                            <div class="recent-comments-item">
                                <div class="row">
                                    <!-- Comments Thumbnails -->
                                    <div class="col-md-4">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                    <!-- Comments Content -->
                                    <div class="col-md-8 no-padding">
                                        <h5><?php echo $comment; ?></h5>
                                        <!-- Comments Date -->
                                        <ul>
                                            <li>
                                                <i class="fa fa-clock-o"></i><?php echo $changeDate; ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Recent Comments Item End -->
<?php
                            }
                            }
                            }
?>

                            <!-- Recent Comments Item End -->

                        </div>
                    </div>

                    <!-- Meta Tag -->
                    <div class="widget">
                        <h4>Tags</h4>
                        <div class="title-border"></div>
                        <!-- Meta Tag List Start -->
                        <div class="meta-tags">

                            <?php 
                                $sql = "SELECT * FROM posts WHERE status = 1 ORDER BY id DESC";
                            /** @var TYPE_NAME $dbConnection */
                            $allTags = mysqli_query($dbConnection, $sql);
                                while( $row = mysqli_fetch_assoc($allTags) ){
                                    $tags = $row['tags'];

                                    // STR to ARRAY
                                    $eachTags = explode(',', trim($tags));
                                    
                                    foreach( $eachTags as $tag ){
                                            
                                            ?>
                                                <span><a href="search.php?s=<?php echo $tag; ?>"><?php echo trim($tag); ?></a></span>
                                            <?php 
                                        
                                                                               
                                    }
                                }

                            ?>

                            

                            
                        </div>
                        <!-- Meta Tag List End -->
                    </div>

                </div>
                <!-- Right Sidebar End -->