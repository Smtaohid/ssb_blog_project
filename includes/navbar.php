<nav class="navbar cuntomnav navbar-expand-lg navbar-light bg-light shadow ">
    <a class="navbar-brand ps-5 font-weight-bolder h1" href="index.php">Blog</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse text-center mx-auto"  id="navbarSupportedContent">
        <ul class="navbar-nav mx-auto ">
            <?php
            $readCategory = "SELECT * FROM categories WHERE is_parent = 0 ORDER BY order_by DESC  LIMIT 6";
            $catReadStmt = mysqli_query($dbConnection,$readCategory);
            $sl = 1;
            while ( $row = mysqli_fetch_assoc($catReadStmt)){
            $id = $row['id'];
            $title = $row['cat_title'];
            $parent_cat = $row['is_parent'];
            $status = $row['status'];
            ?>

                <?php

                $subCatQuery = "SELECT * FROM categories WHERE is_parent = '$id' ";
                $subCat = mysqli_query($dbConnection, $subCatQuery);

                $subCatCountRow = mysqli_num_rows($subCat);
                if ($subCatCountRow){
                    ?>
            <li class="nav-item ">
                <a class="font-weight-bold" href="category.php?catid=<?php echo $id?>">
                    <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle  font-weight-bold menu-item dropdown-menuitem" role="link" target="_blank" href="category.php?catid=<?php echo $id?>" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                        <?php echo $title; ?>
                    </a>

                    <div class="dropdown-menu text-dark menu-item" aria-labelledby="navbarDropdown">
                        <?php

                        $subCatQuery = "SELECT * FROM categories WHERE is_parent = '$id' LIMIT 9 ";
                        $subCat = mysqli_query($dbConnection, $subCatQuery);
                        while ($row = mysqli_fetch_assoc($subCat)){
                            $id = $row['id'];
                            $title = $row['cat_title'];
                            $parent_cat = $row['is_parent'];
                            $status = $row['status'];
                            ?>
                            <a class="dropdown-item  menu-item  font-weight-bold text-dark"   role="link" href="category.php?catid=<?php echo $id?>"><?php echo $title; ?></a>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }else{
                    ?>
                    <a class="dropdown-item  menu-item font-weight-bold" href="category.php?catid=<?php echo $id?>"><?php echo $title; ?></a>
                    <?php
                }
                ?>
                </li>
                </li>

                </a>


            <?php

            }

            ?>

   <li class="nav-item ms-2">

       <?php
       if (isset($_SESSION['id'])){
          $userId = $_SESSION['id'];

          $userNameQuery = "SELECT name FROM users where status = 1 AND  id = '$userId'";
          $userNameStmt = mysqli_query($dbConnection,$userNameQuery);
          while ($row = mysqli_fetch_array($userNameStmt)){
            $userName = $row['name'];

          }
           ?>
            <li class="nav-item dropdown ml-2">

                <a class="nav-link dropdown-toggle font-weight-bold menu-item dropdown-menuitem" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                   <i class="fa fa-user"></i> <?php /** @var TYPE_NAME $userName */
                   echo $userName; ?>
                </a>

            <div class="dropdown-menu menu-item dropdown-menuitem text-center">
                <a class="dropdown-item font-weight-bold menu-item  text-dark" href="user_profile.php?do=profile-view">My Profile</a>
                <a class="dropdown-item font-weight-bold menu-item  text-dark" href="front_logout.php"><i class="fa fa-power-off text-danger"></i> Log Out</a>
                </div>
            </li>

       <?php

       }else{
           ?>
               <li class="nav-item dropdown-menuitem">
                   <a class="nav-link  menu-item  text-dark" href="flogin.php">LogIn</a>
               </li>
           <li class="nav-item dropdown-menuitem">
               <a class="nav-link  d menu-item  text-dark" href="fregister.php">Register</a>

           </li>
       <?php
       }
       ?>

   </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" action="search.php" method="post">
            <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
            <button hidden class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>