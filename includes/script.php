
<!-- JQuery Library File -->
<script type="text/javascript" src="assets/frontend/js/jquery-1.12.4.min.js"></script>
<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script-->
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>

<!-- Bootstrap JS -->
<script src="../assets/frontend/bootstrap/js/bootstrap.min.js"></script>

<!-- Owl Carousel JS -->
<script src="assets/frontend/js/owl.carousel.min.js"></script>

<!-- Isotop JS -->
<script src="assets/frontend/js/isotop.min.js"></script>

<!-- Fency Box JS -->
<script src="assets/frontend/js/jquery.fancybox.min.js"></script>

<!-- Easy Pie Chart JS -->
<script src="assets/frontend/js/jquery.easypiechart.js"></script>

<!-- JQuery CounterUp JS -->
<script src="assets/frontend/js/waypoints.min.js"></script>
<script src="assets/frontend/js/jquery.counterup.min.js"></script>

<!-- BlueChip Extarnal Script -->
<script type="text/javascript" src="assets/frontend/js/main.js"></script>