<?php
ob_start();
include('backend/inc/dbConn.php');
if (empty($_SESSION['id'])) {
    header('front_logout.php');
}
?>



  <!-- Required Meta Tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Website Description -->
  <meta name="description" content="Blue Chip: Corporate Multi Purpose Business Template" />
  <meta name="author" content="Blue Chip" />

  <!--  Favicons / Title Bar Icon  -->
  <link rel="shortcut icon" href="assets/frontend/images/favicon/favicon.png" />
  <link rel="apple-touch-icon-precomposed" href="assets/frontend/images/favicon/favicon.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/frontend/images/favicon/favicon.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" hreassets/frontend/images/favicon/favicon.png" />

  <title>Blue Chip | Blog Right Sidebar</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="assets/frontend/bootstrap/css/bootstrap.min.css">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" type="text/css" href="assets/frontend/css/font-awesome.min.css">

  <!-- Flat Icon CSS -->
  <link rel="stylesheet" type="text/css" href="assets/frontend/css/flaticon.css">

  <!-- Animate CSS -->
  <link rel="stylesheet" type="text/css" href="assets/frontend/css/animate.min.css">

  <!-- Owl Carousel CSS -->
  <link rel="stylesheet" type="text/css" href="assets/frontend/css/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="assets/frontend/css/owl.theme.default.min.css">

  <!-- Fency Box CSS -->
  <link rel="stylesheet" type="text/css" href="assets/frontend/css/jquery.fancybox.min.css">

  <!-- Theme Main Style CSS -->
  <link rel="stylesheet" type="text/css" href="assets/frontend/css/style.css">

  <!-- Responsive CSS -->
  <link rel="stylesheet" type="text/css" href="assets/frontend/css/responsive.css">