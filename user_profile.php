
<!doctype html>
<html lang="en">
<head>
    <?php
    include "includes/header.php";
    ?>
</head>
<body>
<!-- :::::::::: Header Section Start :::::::: -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <?php include "includes/navbar.php"; ?>
            </div>
        </div>
    </div>
</header>


<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column mt-5">
    <!-- Main Content -->
    <div id="content">
        <div class="container">
            <?php
            if (isset($_GET['do'])) {
            $do = $_GET['do'];
            if ($do == "profile-view") {
            ?>



            <div class="row">


                <?php
                $id = $_SESSION['id'];
                $query = "SELECT * FROM users WHERE id = $id";

                $statement = mysqli_query($dbConnection, $query);
                $row = mysqli_fetch_assoc($statement);

                ?>
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h3>My Profile</h3>
                        </div>
                        <div class="card-img-top text-center mt-5">
                            <img class="profile-image" style="width: 200px; height: 200px ;border-radius: 50%;" src="assets/image/upload/users/<?php echo $row['image'] ?>" alt="">
                        </div>
                        <div class="card-body text-center">
                            <h4 class="fw-bolder text-dark profile-name"><?php echo $row['name']; ?></h4>
                            <p class="profile-email"><span class="mx-2"><i class="fa fa-envelope "></i></span><?php echo $row['email']; ?></p>
                            <p class="profile-email"><span class="mx-2"><i class="fa fa-phone "></i></span><?php echo $row['phone']; ?></p>
                            <a class="mt-2 btn btn-main" href="user_profile.php?do=profile-edit">Edit Profile</a>

                        </div>
                        <div class="card-footer d-flex justify-content-between">

                            <address><?php echo $row['address'] ?></address>
                            <p>Joined At <?php echo $row['join_date'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } elseif ($do == 'profile-edit') {
        $id = $_SESSION['id'];
        $query = "SELECT * FROM users WHERE id=$id";
        $statement = mysqli_query($dbConnection, $query);
        $row = mysqli_fetch_assoc($statement);
        $name = $row['name'];
        $email = $row['email'];
        $phone = $row['phone'];
        $address = $row['address'];
        $image = $row['image'];
        ?>
        <div class="container">



            <div class="row">

                <!-- Page Heading -->
                <div class="col-md-12">
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Edit Profile</h1>

                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="error_message">
                                <?php
                                if (!empty($_SESSION['msg'])) {
                                    echo '<div class="alert alert-danger">' . $_SESSION['msg'] . '</div>';
                                    unset($_SESSION['msg']);
                                } elseif (!empty($_SESSION['success_msg'])) {
                                    echo '<div class="alert alert-success">' . $_SESSION['success_msg'] . '</div>';
                                    unset($_SESSION['success_msg']);
                                }
                                ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 mx-auto">

                                    <?php
                                    if ($image) {
                                        ?>

                                        <div class=" text-center edit-profile-img" id="preview">
                                            <img style="height: 200px; width: 200px;" src="assets/image/upload/users/<?php echo $image; ?>" alt="">
                                        </div>

                                        <?php
                                    } else {
                                        ?>

                                        <div class=" text-center edit-profile-img" id="preview">
                                            <img src="assets/image/upload/users/60111.jpg" alt="">
                                        </div>

                                        <?php

                                    }

                                    ?>
                                    <div class=" text-center edit-profile-img" id="preview">

                                    </div>
                                    <form action="user_profile.php?do=profile-update" method="POST" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="name">Name</label>
                                                <input value="<?php echo $name; ?>" type="text" name="name" id="name" class="form-control"></input>


                                                <label class="mt-3" for="address">Address</label>
                                                <input value="<?php echo $address; ?>" type="text" name="address" id="address" class="form-control"></input>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="email">Email</label>
                                                <input value="<?php echo $email; ?>" type="email" name="email" id="email" class="form-control"></input>
                                                <label class="mt-3" for="password">Password</label>
                                                <input placeholder="********" type="password" name="password" id="password" class="form-control"></input>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="phone">phone</label>
                                                <input value="<?php echo $phone; ?>" type="number" name="phone" id="phone" class="form-control"></input>

                                                <label class="mt-3" for="confirn_password">Confirm Password</label>
                                                <input placeholder="********" type="password" name="confirm_password" id="confirn_password" class="form-control"></input>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="image" class=" mt-3">Isert Profile Image </label>

                                                <input type="file" name="image" id="file" onchange="getImagePreview(event)">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="submit" name="update" value="Save Cahnges" class="mt-5 form-control btn btn-primary">
                                            </div>
                                        </div>
                                    </form>

                                </div>

                            </div>

                        </div>
                    </div>

                </div>



            </div>
        </div>
        <?php

    } elseif ($do == 'profile-update') {
        if (isset($_POST['update'])) {
            $userId = mysqli_real_escape_string($dbConnection, $_SESSION['id']);
            $name = mysqli_real_escape_string($dbConnection, $_POST['name']);
            $email =  mysqli_real_escape_string($dbConnection, $_POST['email']);
            $phone = mysqli_real_escape_string($dbConnection, $_POST['phone']);
            $password = mysqli_real_escape_string($dbConnection, $_POST['password']);
            $conf_password = mysqli_real_escape_string($dbConnection, $_POST['confirm_password']);
            $address = mysqli_real_escape_string($dbConnection, $_POST['address']);
            $image = $_FILES['image']['name'];
            $tmpImage = $_FILES['image']['tmp_name'];
            $image = $_FILES['image']['name'];
            $tmpImage = $_FILES['image']['tmp_name'];
            $imageSize = $_FILES['image']['size'];
            $imageType = $_FILES['image']['type'];


            if (empty($password) && empty($image)) {
                $qurey = "UPDATE users SET name='$name',email='$email',phone='$phone',address='$address',role='2',status='1' WHERE id='$userId'";
                $updateStmt = mysqli_query($dbConnection, $qurey);
                if ($updateStmt) {
                    $_SESSION['success_msg'] = "Updated Successfully";
                    header("Location:user_profile.php?do=profile-view");
                } else {
                    die('Not Saved' . mysqli_error($dbConnection));
                }
            } elseif (!empty($password) && !empty($image)) {

                if ($password == $conf_password) {
                    if (strlen($password) <= 7 || strlen($password) > 100) {
                        $_SESSION['msg'] = "Password must be at least 8 characters";
                        header(" Location:user_profile.php?do=profile-edit");
                    } else {
                        $hassedPass = sha1($password);
                        if (!empty($image)) {
                            $imageExtention = strtolower(end(explode('.', $image)));
                            $defineExtentions = array("jpeg", "jpg", "png", "webp");
                            if (in_array($imageExtention, $defineExtentions) == true && $imageSize <= 2097152) {
                                /** @var TYPE_NAME $query */
                                $query = "SELECT * FROM users WHERE id='$userId'";
                                $statement = mysqli_query($dbConnection, $query);
                                $item = mysqli_fetch_assoc($statement);
                                unlink('assets/image/upload/users/' . $item['image']);
                                $imgs = rand(1, 999999999) . '-img-' . $image;
                                move_uploaded_file($tmpImage, 'assets/image/upload/users/' . $imgs);
                                $UpdateQuery = "UPDATE users SET name='$name',email='$email',password='$hassedPass',phone='$phone',address='$address',role='2',status='1',image='$imgs' WHERE id='$userId'";
                                $updateUser = mysqli_query($dbConnection, $UpdateQuery);

                                if ($updateUser) {
                                    $_SESSION['success_msg'] = "Updated Successfully";

                                    header('Location:user_profile.php?do=profile-view');
                                } else {
                                    die('Update Failed' . mysqli_error($dbConnection));
                                }
                            } else {
                                $_SESSION['msg'] = 'You can only upload Image';
                                header('Location:user_profile.php?do=profile-edit');
                            }
                        }
                    }
                } else {
                    $_SESSION['msg'] = "Password Dosen't Match";
                    header("Location:user_profile.php?do=profile-edit");
                }
            } elseif (empty($password) && !empty($image)) {
                if (!empty($image)) {
                    $imageExtention = strtolower(end(explode('.', $image)));
                    $defineExtentions = array("jpeg", "jpg", "png", "webp");
                    if (in_array($imageExtention, $defineExtentions) == true && $imageSize <= 2097152) {
                        $query = "SELECT * FROM users WHERE id='$userId'";
                        $statement = mysqli_query($dbConnection, $query);
                        $item = mysqli_fetch_assoc($statement);
                        unlink('assets/image/upload/users/' . $item['image']);
                        $imgs = rand(1, 999999999) . '-img-' . $image;
                        move_uploaded_file($tmpImage, 'assets/image/upload/users/' . $imgs);
                        $UpdateQuery = "UPDATE users SET name='$name',email='$email',phone='$phone',address='$address',role='2',status='1',image='$imgs' WHERE id='$userId'";
                        $updateUser = mysqli_query($dbConnection, $UpdateQuery);

                        if ($updateUser) {
                            $_SESSION['success_msg'] = "Updated Successfully";

                            header('Location:user_profile.php?do=profile-view');
                        } else {
                            die('Update Failed' . mysqli_error($dbConnection));
                        }
                    } else {
                        $_SESSION['msg'] = 'You can only upload Image';
                        header('Location:user_profile.php?do=profile-edit');
                    }
                } else {
                    $_SESSION['msg'] = 'Plase Upload Your Profile Image';
                    header('Location:user_profile.php?do=profile-edit');
                }
            } elseif (!empty($password) && empty($image)) {
                if ($password == $conf_password) {

                    if (strlen($password) <= 7 || strlen($password) > 100) {
                        $_SESSION['msg'] = "Password must be at least 8 characters";
                        header("Location:user_profile.php?do=profile-edit");
                    } else {
                        $hassedPass = sha1($password);
                        $UpdateQuery = "UPDATE users SET name='$name',email='$email',phone='$phone',password='$hassedPass',address='$address',role='2',status='1'WHERE id='$userId'";
                        $updateUser = mysqli_query($dbConnection, $UpdateQuery);

                        if ($updateUser) {

                            $_SESSION['success_msg'] = "Updated Successfully";

                            header('Location:user_profile.php?do=profile-view');
                        } else {
                            die('Update Failed' . mysqli_error($dbConnection));
                        }
                    }
                } else {
                    $_SESSION['msg'] = "Password Does Not Match";
                    header("Location:user_profile.php?do=profile-edit");
                }
            }
        }
    }
    } else {
        ?>
        <div class="alert alert-danger">
            Page Can Not Found
        </div>
        <?php
    }
    ?>
    </div><!-- :::::::::: Footer Section Start :::::::: -->
<footer class="my-5">
    <?php
    include "includes/footer.php";
    ?>
</footer>
<!-- ::::::::::: Footer Section End ::::::::: -->

<?php include 'includes/script.php'; ?>
</body>
</html>
